﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace FM5092_A1
{
    //We creat an options class, and will use it as a parent class for all kinds of options.
    public class Options
    {
        Stopwatch watch = new Stopwatch();
        //We difined several property of the objects in the option class.
        //Isput indicates that if this objects is put option or call option.
        public bool isput = false;
        //S0 is the underlying price, K is the strike price, r is the risk free rate, sigma is volatility, T is tenor.
        public double S0, K, r, sigma, T;
        //In order to calculate greek values, I set the random variables matrix R as a property for now. 
        //Actually I don't think this is a good idea, and I will find some way to improve this.
        public double[,] R1;
        //This method is used to get the results of the Simulator.Simulate method.
        public int Thread;
        public double[,] Getsim(int steps, int N, Boolean IsMT)
        {
            double[,] sims;
            if (IsMT == false)
            {
                Thread = 1;
            }
            else
            {
                Thread = System.Environment.ProcessorCount;
            }
            sims = Simulator.Simulate(S0, sigma, r, T, steps, N, R1, Thread);
            return sims;
        }
        public double[,] Getsim2(int steps, int N, Boolean IsMT)
        {
            double[,] sims2;
            double[,] R2 = new double[N, steps + 1];

            for (int j = 0; j < N; j++)
            {
                for (int i = 1; i <= steps; i++)
                {
                    R2[j, i - 1] = -R1[j, i - 1];
                }
            }
            if (IsMT == false)
            {
                Thread = 1;
            }
            else
            {
                Thread = System.Environment.ProcessorCount;
            }
            sims2 = Simulator.Simulate(S0, sigma, r, T, steps, N, R2, Thread);
            return sims2;
        }

        public void scheduler()
        {
            watch.Start();
            Summary sum1 = new Summary();
            sum1.Sum();
            Program.increase(1);
            watch.Stop();
            IO.Time = watch.Elapsed.Hours.ToString() + " h " + watch.Elapsed.Minutes.ToString() + " m "
                + watch.Elapsed.Seconds.ToString() + " s " + watch.Elapsed.Milliseconds.ToString() + " ms ";
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
    }

    //We created another class named EuroOption, which inherited the parent class Options.
    public class EuroOption : Options
    {
        //This method is used to calculate the European option price.
        public double PriceEuro(int steps, int N, Boolean IsAnti, Boolean IsCV, Boolean IsMT)
        {
            double sum = 0, price = 0;
            //We used the simulation matrix we obtained.
            double[,] sims;
            sims = Getsim(steps, N, IsMT);
            if (IsAnti == false && IsCV == false)
            {
                for (int i = 0; i < N; i++)
                {
                    if (isput == false)
                    {
                        //Gives the option price of call option.
                        sum += Math.Max(sims[i, steps] - K, 0);
                    }
                    else
                    {
                        //Gives the option price of put option.
                        sum += Math.Max(K - sims[i, steps], 0);
                    }
                }
                price = (sum / N) * Math.Exp(-r * T);
            }
            else if (IsAnti == true && IsCV == false)
            {
                double[,] sims2;
                sims2 = Getsim2(steps, N, IsMT);
                for (int i = 0; i < N; i++)
                {
                    if (isput == false)
                    {
                        sum += Math.Max(sims[i, steps] - K, 0);
                        sum += Math.Max(sims2[i, steps] - K, 0);
                    }
                    else
                    {
                        sum += Math.Max(K - sims[i, steps], 0);
                        sum += Math.Max(K - sims2[i, steps], 0);
                    }
                    price = (sum / N / 2) * Math.Exp(-r * T);
                }
            }
            else if (IsAnti == false && IsCV == true)
            {
                double dt = T / steps, beta1 = -1;
                double nudt = (r - sigma * sigma / 2) * dt;
                double sigsdt = sigma * Math.Sqrt(dt);
                double erddt = Math.Exp(r * dt);
                //double[] CT = new double[N];

                double CT;
                double cv, St, Stn;
                double sum_CT = 0, sum_CT2 = 0;
                double[] sum_CT_a = new double[N];
                double[] sum_CT2_a = new double[N];
                double t, delta;
                //Parallel.ForEach(IEnum.Step(0, N, 1), new ParallelOptions { MaxDegreeOfParallelism = IO.Thread }, i =>
                for (int i = 0; i < N; i++)
                {
                    St = S0;
                    cv = 0;
                    for (int j = 1; j <= steps; j++)
                    {
                        t = (j - 1) * dt;
                        delta = BlackScholes.Delta(isput, St, t, K, sigma, T, r);
                        Stn = St * Math.Exp(nudt + sigsdt * R1[i, j - 1]);
                        cv = cv + delta * (Stn - St * erddt);
                        St = Stn;
                    }

                    if (isput == false)
                    {
                        CT = Math.Max(St - K, 0) + beta1 * cv;
                    }
                    else
                    {
                        CT = Math.Max(-St + K, 0) + beta1 * cv;
                    }
                    sum_CT_a[i] = CT;
                    sum_CT2_a[i] = CT * CT;
                }
                for (int i = 0; i < N; i++)
                {
                    sum_CT = sum_CT + sum_CT_a[i];
                    sum_CT2 = sum_CT2 + sum_CT2_a[i];
                }
                
                price = sum_CT / N * Math.Exp(-r * T);                
            }
            else if(IsAnti== true && IsCV == true)
            {
                double dt = T / steps, beta1 = -1;
                double nudt = (r - sigma * sigma / 2) * dt;
                double sigsdt = sigma * Math.Sqrt(dt);
                double erddt = Math.Exp(r * dt);

                double sum_CT = 0, sum_CT2 = 0;
                double St1, St2, cv1, cv2, t;
                double delta1, delta2, Stn1, Stn2;
                double CT;
                //Parallel.ForEach(IEnum.Step(0, N, 1), new ParallelOptions { MaxDegreeOfParallelism = IO.Thread }, i =>
                for (int i = 0; i < N; i++)
                {
                    St1 = S0;
                    St2 = S0;
                    cv1 = 0;
                    cv2 = 0;
                    for (int j = 1; j <= steps; j++)
                    {
                        t = (j - 1) * dt;
                        delta1 = BlackScholes.Delta(isput, St1, t, K, sigma, T, r);
                        delta2 = BlackScholes.Delta(isput, St2, t, K, sigma, T, r);
                        Stn1 = St1 * Math.Exp(nudt + sigsdt * R1[i, j - 1]);
                        Stn2 = St2 * Math.Exp(nudt + sigsdt * (-R1[i, j - 1]));
                        cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                        cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);
                        St1 = Stn1;
                        St2 = Stn2;
                    }
                    if (isput == false)
                    {
                        CT = 0.5 * (Math.Max(0, St1 - K) + beta1 * cv1 + Math.Max(0, St2 - K) + beta1 * cv2);
                    }
                    else
                    {
                        CT = 0.5 * (Math.Max(0, -St1 + K) + beta1 * cv1 + Math.Max(0, -St2 + K) + beta1 * cv2);
                    }
                    sum_CT = sum_CT + CT;
                    sum_CT2 = sum_CT2 + CT * CT;
                }
                price = sum_CT / N * Math.Exp(-r * T);
            }
            return price;
        }
        //This method is used to calculate the standard error.
        public double EuroSE(int steps, int N, Boolean IsAnti, Boolean IsCV, Boolean IsMT)
        {
            double[,] sims;
            sims = Getsim(steps, N, IsMT);
            double sums = 0, Std=0;
            double price = PriceEuro(steps, N, IsAnti,IsCV,IsMT);
            if (IsAnti == false && IsCV==false)
            {
                for (int i = 0; i < N; i++)
                {
                    if (isput == false)
                    {
                        sums += Math.Pow((Math.Max(sims[i, steps] - K, 0) * Math.Exp(-r * T) - price), 2);
                    }
                    else
                    {
                        sums += Math.Pow((Math.Max(K - sims[i, steps], 0) * Math.Exp(-r * T) - price), 2);
                    }
                }
                Std = Math.Sqrt(sums / (N - 1) / N);
            }
            else if(IsAnti==true && IsCV==false)
            {
                double[,] sims2;
                sims2 = Getsim2(steps, N, IsMT);
                for (int i = 0; i < N; i++)
                {
                    if (isput == false)
                    {
                        sums += Math.Pow(((Math.Max(sims[i, steps] - K, 0) + Math.Max(sims2[i, steps] - K, 0)) / 2 * Math.Exp(-r * T) - price), 2);
                    }
                    else
                    {
                        sums += Math.Pow(((Math.Max(K - sims[i, steps], 0) + Math.Max(K - sims2[i, steps], 0)) / 2 * Math.Exp(-r * T) - price), 2);
                    }
                }
                Std = Math.Sqrt(sums / (N - 1) / N);
            }
            else if (IsAnti == false && IsCV == true)
            {
                double dt = T / steps, beta1 = -1;
                double nudt = (r - sigma * sigma / 2) * dt;
                double sigsdt = sigma * Math.Sqrt(dt);
                double erddt = Math.Exp(r * dt);

                double cv,St,Stn;
                double CT, sum_CT = 0, sum_CT2 = 0;
                double[] CT0 = new double[N];
                double t, delta;
                //Parallel.ForEach(IEnum.Step(0, N, 1), new ParallelOptions { MaxDegreeOfParallelism = Thread }, i =>
                for (int i = 0; i < N; i++)
                {
                    St = S0;
                    cv = 0;
                    for (int j = 1; j <= steps; j++)
                    {
                        t = (j - 1) * dt;
                        delta = BlackScholes.Delta(isput, St, t, K, sigma, T, r);
                        Stn = St * Math.Exp(nudt + sigsdt * R1[i, j - 1]);
                        cv = cv + delta * (Stn - St * erddt);
                        St = Stn;
                    }

                    if (isput == false)
                    {
                        CT = Math.Max(St - K, 0) + beta1 * cv;
                    }
                    else
                    {
                        CT = Math.Max(-St + K, 0) + beta1 * cv;
                    }
                    //sum_CT = sum_CT + CT;
                    //sum_CT2 = sum_CT2 + CT * CT;
                    CT0[i] = CT;
                }
                for (int i = 0; i < N; i++)
                {
                    sum_CT = sum_CT + CT0[i];
                    sum_CT2 = sum_CT2 + CT0[i] * CT0[i];
                }
                Std = Math.Sqrt((sum_CT2 - sum_CT * sum_CT / N) * Math.Exp(-2 * r * T) / (N - 1)) / Math.Sqrt(N);
            }
            else if (IsAnti == true && IsCV == true)
            {
                double dt = T / steps, beta1 = -1;
                double nudt = (r - sigma * sigma / 2) * dt;
                double sigsdt = sigma * Math.Sqrt(dt);
                double erddt = Math.Exp(r * dt);

                double sum_CT = 0, sum_CT2 = 0;
                double St1, St2, cv1, cv2, t;
                double delta1, delta2, Stn1, Stn2;
                double CT;

                //Parallel.ForEach(IEnum.Step(0, N, 1), new ParallelOptions { MaxDegreeOfParallelism = Thread }, i =>
                for (int i = 0; i < N; i++)
                {
                    St1 = S0;
                    St2 = S0;
                    cv1 = 0;
                    cv2 = 0;
                    for (int j = 1; j <= steps; j++)
                    {
                        t = (j - 1) * dt;
                        delta1 = BlackScholes.Delta(isput, St1, t, K, sigma, T, r);
                        delta2 = BlackScholes.Delta(isput, St2, t, K, sigma, T, r);
                        Stn1 = St1 * Math.Exp(nudt + sigsdt * R1[i, j - 1]);
                        Stn2 = St2 * Math.Exp(nudt + sigsdt * (-R1[i, j - 1]));
                        cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                        cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);
                        St1 = Stn1;
                        St2 = Stn2;
                    }
                    if (isput == false)
                    {
                        CT = 0.5 * (Math.Max(0, St1 - K) + beta1 * cv1 + Math.Max(0, St2 - K) + beta1 * cv2);
                    }
                    else
                    {
                        CT = 0.5 * (Math.Max(0, -St1 + K) + beta1 * cv1 + Math.Max(0, -St2 + K) + beta1 * cv2);
                    }
                    sum_CT = sum_CT + CT;
                    sum_CT2 = sum_CT2 + CT * CT;
                }
                double SD = Math.Sqrt((sum_CT2 - sum_CT * sum_CT / N) * Math.Exp(-2 * r * T) / (N - 1));
                Std = SD / Math.Sqrt(N);
            }
            return Std;
        }
    }

    public class AsianOption : Options
    {
        public double PriceAsian(int steps, int N, Boolean IsAnti, Boolean IsCV, Boolean IsMT)
        {
            double sum = 0, price = 0;
            double S_sum = 0;
            double[] S_average1 = new double[N];
            //We used the simulation matrix we obtained.
            double[,] sims;
            sims = Getsim(steps, N, IsMT);
            for(int i = 0; i < N; i++)
            {
                S_sum = 0;
                for(int j = 0; j < steps; j++)
                {
                    S_sum = S_sum + sims[i, j];
                }
                S_average1[i] = S_sum / steps;
            }
            if (IsAnti == false && IsCV == false)
            {
                for (int i = 0; i < N; i++)
                {
                    if (isput == false)
                    {
                        //Gives the option price of call option.
                        sum += Math.Max(S_average1[i] - K, 0);
                    }
                    else
                    {
                        //Gives the option price of put option.
                        sum += Math.Max(K - S_average1[i], 0);
                    }
                }
                price = (sum / N) * Math.Exp(-r * T);
            }
            else if (IsAnti == true && IsCV == false)
            {
                double[,] sims2;
                sims2 = Getsim2(steps, N, IsMT);
                double S_sum2 = 0;
                double[] S_average2 = new double[N];
                for (int i = 0; i < N; i++)
                {
                    S_sum2 = 0;
                    for(int j = 0; j < steps; j++)
                    {
                        S_sum2 = S_sum2 + sims2[i, j];
                    }
                    S_average2[i] = S_sum2 / steps;
                }

                for (int i = 0; i < N; i++)
                {
                    if (isput == false)
                    {
                        sum += Math.Max(S_average1[i] - K, 0);
                        sum += Math.Max(S_average2[i] - K, 0);
                    }
                    else
                    {
                        sum += Math.Max(K - S_average1[i], 0);
                        sum += Math.Max(K - S_average2[i], 0);
                    }
                    price = (sum / N / 2) * Math.Exp(-r * T);
                }
            }
            else if (IsAnti == false && IsCV == true)
            {
                double dt = T / steps, beta1 = -1;
                double nudt = (r - sigma * sigma / 2) * dt;
                double sigsdt = sigma * Math.Sqrt(dt);
                double erddt = Math.Exp(r * dt);
                double St_sum;

                double cv, St, Stn;
                double CT, sum_CT = 0, sum_CT2 = 0;
                double[] CT0 = new double[N];
                double t, delta;
                //Parallel.ForEach(IEnum.Step(0, N, 1), new ParallelOptions { MaxDegreeOfParallelism = Thread }, i =>
                for (int i = 0; i < N; i++)
                {
                    St = S0;
                    cv = 0;
                    St_sum = St;
                    for (int j = 1; j <= steps; j++)
                    {
                        t = (j - 1) * dt;
                        delta = BlackScholes.Delta(isput, St, t, K, sigma, T, r);
                        Stn = St * Math.Exp(nudt + sigsdt * R1[i, j - 1]);
                        cv = cv + delta * (Stn - St * erddt);
                        St = Stn;
                        St_sum = St_sum + St;
                    }

                    if (isput == false)
                    {
                        CT = Math.Max(St_sum / steps - K, 0) + beta1 * cv;
                    }
                    else
                    {
                        CT = Math.Max(-St_sum / steps + K, 0) + beta1 * cv;
                    }
                    //sum_CT = sum_CT + CT;
                    //sum_CT2 = sum_CT2 + CT * CT;
                    CT0[i] = CT;
                }
                for (int i = 0; i < N; i++)
                {
                    sum_CT = sum_CT + CT0[i];
                    sum_CT2 = sum_CT2 + CT0[i] * CT0[i];
                }
                price = sum_CT / N * Math.Exp(-r * T);
            }
            else if (IsAnti == true && IsCV == true)
            {
                double[,] sims2;
                sims2 = Getsim2(steps, N, IsMT);
                double S_sum2 = 0;
                double[] S_average2 = new double[N];
                for (int i = 0; i < N; i++)
                {
                    S_sum2 = 0;
                    for (int j = 0; j < steps; j++)
                    {
                        S_sum2 = S_sum2 + sims2[i, j];
                    }
                    S_average2[i] = S_sum2 / steps;
                }

                double dt = T / steps, beta1 = -1;
                double nudt = (r - sigma * sigma / 2) * dt;
                double sigsdt = sigma * Math.Sqrt(dt);
                double erddt = Math.Exp(r * dt);

                double sum_CT = 0, sum_CT2 = 0;
                double St1, St2, cv1, cv2, t;
                double delta1, delta2, Stn1, Stn2;
                double CT;

                //Parallel.ForEach(IEnum.Step(0, N, 1), new ParallelOptions { MaxDegreeOfParallelism = Thread }, i =>
                for (int i = 0; i < N; i++)
                {
                    St1 = S0;
                    St2 = S0;
                    cv1 = 0;
                    cv2 = 0;
                    for (int j = 1; j <= steps; j++)
                    {
                        t = (j - 1) * dt;
                        delta1 = BlackScholes.Delta(isput, St1, t, K, sigma, T, r);
                        delta2 = BlackScholes.Delta(isput, St2, t, K, sigma, T, r);
                        Stn1 = St1 * Math.Exp(nudt + sigsdt * R1[i, j - 1]);
                        Stn2 = St2 * Math.Exp(nudt + sigsdt * (-R1[i, j - 1]));
                        cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                        cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);
                        St1 = Stn1;
                        St2 = Stn2;
                    }
                    if (isput == false)
                    {
                        CT = 0.5 * (Math.Max(0, S_average1[i] - K) + beta1 * cv1 + Math.Max(0, S_average2[i] - K) + beta1 * cv2);
                    }
                    else
                    {
                        CT = 0.5 * (Math.Max(0, -S_average1[i] + K) + beta1 * cv1 + Math.Max(0, -S_average2[i] + K) + beta1 * cv2);
                    }
                    sum_CT = sum_CT + CT;
                    sum_CT2 = sum_CT2 + CT * CT;
                }
                price = sum_CT / N * Math.Exp(-r * T);
            }
            
            return price;
        }

        public double AsianSE(int steps, int N, Boolean IsAnti, Boolean IsCV, Boolean IsMT)
        {
            double[,] sims;
            sims = Getsim(steps, N, IsMT);

            double sums = 0, Std = 0, S_sum=0;
            double price = PriceAsian(steps, N, IsAnti, IsCV, IsMT);
            double[] S_average1 = new double[N];
            //We used the simulation matrix we obtained.
            for (int i = 0; i < N; i++)
            {
                S_sum = 0;
                for (int j = 0; j < steps; j++)
                {
                    S_sum = S_sum + sims[i, j];
                }
                S_average1[i] = S_sum / steps;
            }

            if (IsAnti == false && IsCV == false)
            {
                for (int i = 0; i < N; i++)
                {
                    if (isput == false)
                    {
                        sums += Math.Pow((Math.Max(S_average1[i] - K, 0) * Math.Exp(-r * T) - price), 2);
                    }
                    else
                    {
                        sums += Math.Pow((Math.Max(K - S_average1[i], 0) * Math.Exp(-r * T) - price), 2);
                    }
                }
                Std = Math.Sqrt(sums / (N - 1) / N);
            }
            else if (IsAnti == true && IsCV == false)
            {
                double[,] sims2;
                sims2 = Getsim2(steps, N, IsMT);
                double S_sum2 = 0;
                double[] S_average2 = new double[N];
                for (int i = 0; i < N; i++)
                {
                    S_sum2 = 0;
                    for (int j = 0; j < steps; j++)
                    {
                        S_sum2 = S_sum2 + sims2[i, j];
                    }
                    S_average2[i] = S_sum2 / steps;
                }
                for (int i = 0; i < N; i++)
                {
                    if (isput == false)
                    {
                        sums += Math.Pow(((Math.Max(S_average1[i] - K, 0) + Math.Max(S_average2[i] - K, 0)) / 2 * Math.Exp(-r * T) - price), 2);
                    }
                    else
                    {
                        sums += Math.Pow(((Math.Max(K - S_average1[i], 0) + Math.Max(K - S_average2[i], 0)) / 2 * Math.Exp(-r * T) - price), 2);
                    }
                }
                Std = Math.Sqrt(sums / (N - 1) / N);
            }
            else if (IsAnti == false && IsCV == true)
            {
                double dt = T / steps, beta1 = -1;
                double nudt = (r - sigma * sigma / 2) * dt;
                double sigsdt = sigma * Math.Sqrt(dt);
                double erddt = Math.Exp(r * dt);
                double St_sum ;

                double cv, St, Stn;
                double CT, sum_CT = 0, sum_CT2 = 0;
                double[] CT0 = new double[N];
                double t, delta;
                //Parallel.ForEach(IEnum.Step(0, N, 1), new ParallelOptions { MaxDegreeOfParallelism = Thread }, i =>
                for (int i = 0; i < N; i++)
                {
                    St = S0;
                    cv = 0;
                    St_sum = St;
                    for (int j = 1; j <= steps; j++)
                    {
                        t = (j - 1) * dt;
                        delta = BlackScholes.Delta(isput, St, t, K, sigma, T, r);
                        Stn = St * Math.Exp(nudt + sigsdt * R1[i, j - 1]);
                        cv = cv + delta * (Stn - St * erddt);
                        St = Stn;
                        St_sum = St_sum + St;
                    }

                    if (isput == false)
                    {
                        CT = Math.Max(St_sum/steps - K, 0) + beta1 * cv;
                    }
                    else
                    {
                        CT = Math.Max(-St_sum/steps + K, 0) + beta1 * cv;
                    }
                    //sum_CT = sum_CT + CT;
                    //sum_CT2 = sum_CT2 + CT * CT;
                    CT0[i] = CT;
                }
                for (int i = 0; i < N; i++)
                {
                    sum_CT = sum_CT + CT0[i];
                    sum_CT2 = sum_CT2 + CT0[i] * CT0[i];
                }
                Std = Math.Sqrt((sum_CT2 - sum_CT * sum_CT / N) * Math.Exp(-2 * r * T) / (N - 1)) / Math.Sqrt(N);
            }
            else if (IsAnti == true && IsCV == true)
            {
                double[,] sims2;
                sims2 = Getsim2(steps, N, IsMT);
                double S_sum2 = 0;
                double[] S_average2 = new double[N];
                for (int i = 0; i < N; i++)
                {
                    S_sum2 = 0;
                    for (int j = 0; j < steps; j++)
                    {
                        S_sum2 = S_sum2 + sims2[i, j];
                    }
                    S_average2[i] = S_sum2 / steps;
                }

                double dt = T / steps, beta1 = -1;
                double nudt = (r - sigma * sigma / 2) * dt;
                double sigsdt = sigma * Math.Sqrt(dt);
                double erddt = Math.Exp(r * dt);

                double sum_CT = 0, sum_CT2 = 0;
                double St1, St2, cv1, cv2, t;
                double delta1, delta2, Stn1, Stn2;
                double CT;

                //Parallel.ForEach(IEnum.Step(0, N, 1), new ParallelOptions { MaxDegreeOfParallelism = Thread }, i =>
                for (int i = 0; i < N; i++)
                {
                    St1 = S0;
                    St2 = S0;
                    cv1 = 0;
                    cv2 = 0;
                    for (int j = 1; j <= steps; j++)
                    {
                        t = (j - 1) * dt;
                        delta1 = BlackScholes.Delta(isput, St1, t, K, sigma, T, r);
                        delta2 = BlackScholes.Delta(isput, St2, t, K, sigma, T, r);
                        Stn1 = St1 * Math.Exp(nudt + sigsdt * R1[i, j - 1]);
                        Stn2 = St2 * Math.Exp(nudt + sigsdt * (-R1[i, j - 1]));
                        cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                        cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);
                        St1 = Stn1;
                        St2 = Stn2;
                    }
                    if (isput == false)
                    {
                        CT = 0.5 * (Math.Max(0, S_average1[i] - K) + beta1 * cv1 + Math.Max(0, S_average2[i] - K) + beta1 * cv2);
                    }
                    else
                    {
                        CT = 0.5 * (Math.Max(0, -S_average1[i] + K) + beta1 * cv1 + Math.Max(0, -S_average2[i] + K) + beta1 * cv2);
                    }
                    sum_CT = sum_CT + CT;
                    sum_CT2 = sum_CT2 + CT * CT;
                }
                double SD = Math.Sqrt((sum_CT2 - sum_CT * sum_CT / N) * Math.Exp(-2 * r * T) / (N - 1));
                Std = SD / Math.Sqrt(N);
            }

            return Std;
        }

    }

    public class DigitalOption : Options
    {
        public double rebate;

        public double PriceDigital(int steps, int N, Boolean IsAnti, Boolean IsCV, Boolean IsMT)
        {
            double sum = 0, price = 0;
            //We used the simulation matrix we obtained.
            double[,] sims;
            sims = Getsim(steps, N, IsMT);
            if (IsAnti == false && IsCV == false)
            {
                for (int i = 0; i < N; i++)
                {
                    if (isput == false)
                    {
                        if (sims[i, steps] > K)
                        {
                            sum = sum + rebate;
                        }
                    }
                    else
                    {
                        if (sims[i, steps] < K)
                        {
                            sum = sum + rebate;
                        }
                    }
                }
                price = (sum / N) * Math.Exp(-r * T);
            }
            else if (IsAnti == true && IsCV == false)
            {
                double[,] sims2;
                sims2 = Getsim2(steps, N, IsMT);
                for (int i = 0; i < N; i++)
                {
                    if (isput == false)
                    {
                        if (sims[i, steps] > K)
                        {
                            sum = sum + rebate;
                        }
                        if (sims2[i, steps] > K)
                        {
                            sum = sum + rebate;
                        }
                    }
                    else
                    {
                        if (sims[i, steps] < K)
                        {
                            sum = sum + rebate;
                        }
                        if (sims2[i, steps] < K)
                        {
                            sum = sum + rebate;
                        }
                    }
                    price = (sum / N / 2) * Math.Exp(-r * T);
                }
            }
            else if (IsAnti == false && IsCV == true)
            {
                double dt = T / steps, beta1 = -1;
                double nudt = (r - sigma * sigma / 2) * dt;
                double sigsdt = sigma * Math.Sqrt(dt);
                double erddt = Math.Exp(r * dt);

                double cv, St, Stn;
                double CT, sum_CT = 0, sum_CT2 = 0;
                double[] CT0 = new double[N];
                double t, delta;
                //Parallel.ForEach(IEnum.Step(0, N, 1), new ParallelOptions { MaxDegreeOfParallelism = Thread }, i =>
                for (int i = 0; i < N; i++)
                {
                    St = S0;
                    cv = 0;
                    for (int j = 1; j <= steps; j++)
                    {
                        t = (j - 1) * dt;
                        delta = BlackScholes.Delta(isput, St, t, K, sigma, T, r);
                        Stn = St * Math.Exp(nudt + sigsdt * R1[i, j - 1]);
                        cv = cv + delta * (Stn - St * erddt);
                        St = Stn;
                    }

                    if (isput == false)
                    {
                        if (St > K)
                        {
                            CT = rebate + beta1 * cv;
                        }
                        else
                        {
                            CT = beta1 * cv;
                        }
                    }
                    else
                    {
                        if (St < K)
                        {
                            CT = rebate + beta1 * cv;
                        }
                        else
                        {
                            CT = beta1 * cv;
                        }
                    }
                    //sum_CT = sum_CT + CT;
                    //sum_CT2 = sum_CT2 + CT * CT;
                    CT0[i] = CT;
                }
                for (int i = 0; i < N; i++)
                {
                    sum_CT = sum_CT + CT0[i];
                    sum_CT2 = sum_CT2 + CT0[i] * CT0[i];
                }
                price = sum_CT / N * Math.Exp(-r * T);
            }
            else if (IsAnti == true && IsCV == true)
            {
                double dt = T / steps, beta1 = -1;
                double nudt = (r - sigma * sigma / 2) * dt;
                double sigsdt = sigma * Math.Sqrt(dt);
                double erddt = Math.Exp(r * dt);

                double sum_CT = 0, sum_CT2 = 0;
                double St1, St2, cv1, cv2, t;
                double delta1, delta2, Stn1, Stn2;
                double CT;
                double a, b;

                //Parallel.ForEach(IEnum.Step(0, N, 1), new ParallelOptions { MaxDegreeOfParallelism = Thread }, i =>
                for (int i = 0; i < N; i++)
                {
                    St1 = S0;
                    St2 = S0;
                    cv1 = 0;
                    cv2 = 0;
                    for (int j = 1; j <= steps; j++)
                    {
                        t = (j - 1) * dt;
                        delta1 = BlackScholes.Delta(isput, St1, t, K, sigma, T, r);
                        delta2 = BlackScholes.Delta(isput, St2, t, K, sigma, T, r);
                        Stn1 = St1 * Math.Exp(nudt + sigsdt * R1[i, j - 1]);
                        Stn2 = St2 * Math.Exp(nudt + sigsdt * (-R1[i, j - 1]));
                        cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                        cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);
                        St1 = Stn1;
                        St2 = Stn2;
                    }

                    if (isput == false)
                    {
                        a = 0;
                        b = 0;
                        if (St1 > K)
                        {
                            a = rebate;
                        }
                        if (St2 > K)
                        {
                            b = rebate;
                        }
                        CT = 0.5 * (a + beta1 * cv1 + b + beta1 * cv2);
                    }
                    else
                    {

                        a = 0;
                        b = 0;
                        if (St1 < K)
                        {
                            a = rebate;
                        }
                        if (St2 < K)
                        {
                            b = rebate;
                        }
                        CT = 0.5 * (a + beta1 * cv1 + b + beta1 * cv2);
                    }
                    sum_CT = sum_CT + CT;
                    sum_CT2 = sum_CT2 + CT * CT;
                }
                price = sum_CT / N * Math.Exp(-r * T);
            }
            return price;
        }

        public double DigitalSE(int steps, int N, Boolean IsAnti, Boolean IsCV, Boolean IsMT)
        {
            double[,] sims;
            sims = Getsim(steps, N, IsMT);
            double sums = 0, Std = 0;
            double a, b;
            double price = PriceDigital(steps, N, IsAnti, IsCV, IsMT);
            if (IsAnti == false && IsCV == false)
            {
                for (int i = 0; i < N; i++)
                {
                    if (isput == false)
                    {
                        a = 0;
                        if (sims[i, steps] > K) 
                        {
                            a = rebate;
                        }
                        sums += Math.Pow((Math.Max(a, 0) * Math.Exp(-r * T) - price), 2);                     
                    }
                    else
                    {
                        a = 0;
                        if (sims[i, steps] < K)
                        {
                            a = rebate;
                        }
                         sums += Math.Pow((Math.Max(rebate, 0) * Math.Exp(-r * T) - price), 2);                       
                    }
                }
                Std = Math.Sqrt(sums / (N - 1) / N);
            }
            else if (IsAnti == true && IsCV == false)
            {
                double[,] sims2;
                sims2 = Getsim2(steps, N, IsMT);
                for (int i = 0; i < N; i++)
                {
                    if (isput == false)
                    {
                        a = 0;
                        b = 0;
                        if (sims[i, steps] > K)
                        {
                            a = rebate;
                        }
                        if (sims2[i, steps] > K)
                        {
                            b = rebate;
                        }
                        sums += Math.Pow(((a + b) / 2 * Math.Exp(-r * T) - price), 2);
                    }
                    else
                    {
                        a = 0;
                        b = 0;
                        if (sims[i, steps] < K)
                        {
                            a = rebate;
                        }
                        if (sims2[i, steps] < K)
                        {
                            b = rebate;
                        }
                        sums += Math.Pow(((a + b) / 2 * Math.Exp(-r * T) - price), 2);
                    }
                }
                Std = Math.Sqrt(sums / (N - 1) / N);
            }
            else if (IsAnti == false && IsCV == true)
            {
                double dt = T / steps, beta1 = -1;
                double nudt = (r - sigma * sigma / 2) * dt;
                double sigsdt = sigma * Math.Sqrt(dt);
                double erddt = Math.Exp(r * dt);

                double cv, St, Stn;
                double CT, sum_CT = 0, sum_CT2 = 0;
                double[] CT0 = new double[N];
                double t, delta;
                //Parallel.ForEach(IEnum.Step(0, N, 1), new ParallelOptions { MaxDegreeOfParallelism = Thread }, i =>
                for (int i = 0; i < N; i++)
                {
                    St = S0;
                    cv = 0;
                    for (int j = 1; j <= steps; j++)
                    {
                        t = (j - 1) * dt;
                        delta = BlackScholes.Delta(isput, St, t, K, sigma, T, r);
                        Stn = St * Math.Exp(nudt + sigsdt * R1[i, j - 1]);
                        cv = cv + delta * (Stn - St * erddt);
                        St = Stn;
                    }

                    if (isput == false)
                    {
                        if (St > K)
                        {
                            CT = rebate + beta1 * cv;
                        }
                        else
                        {
                            CT = beta1 * cv;
                        }
                    }
                    else
                    {
                        if (St < K)
                        {
                            CT = rebate + beta1 * cv;
                        }
                        else
                        {
                            CT = beta1 * cv;
                        }
                    }
                    //sum_CT = sum_CT + CT;
                    //sum_CT2 = sum_CT2 + CT * CT;
                    CT0[i] = CT;
                }
                for (int i = 0; i < N; i++)
                {
                    sum_CT = sum_CT + CT0[i];
                    sum_CT2 = sum_CT2 + CT0[i] * CT0[i];
                }
                Std = Math.Sqrt((sum_CT2 - sum_CT * sum_CT / N) * Math.Exp(-2 * r * T) / (N - 1)) / Math.Sqrt(N);
            }
            else if (IsAnti == true && IsCV == true)
            {
                double dt = T / steps, beta1 = -1;
                double nudt = (r - sigma * sigma / 2) * dt;
                double sigsdt = sigma * Math.Sqrt(dt);
                double erddt = Math.Exp(r * dt);

                double sum_CT = 0, sum_CT2 = 0;
                double St1, St2, cv1, cv2, t;
                double delta1, delta2, Stn1, Stn2;
                double CT;

                //Parallel.ForEach(IEnum.Step(0, N, 1), new ParallelOptions { MaxDegreeOfParallelism = Thread }, i =>
                for (int i = 0; i < N; i++)
                {
                    St1 = S0;
                    St2 = S0;
                    cv1 = 0;
                    cv2 = 0;
                    for (int j = 1; j <= steps; j++)
                    {
                        t = (j - 1) * dt;
                        delta1 = BlackScholes.Delta(isput, St1, t, K, sigma, T, r);
                        delta2 = BlackScholes.Delta(isput, St2, t, K, sigma, T, r);
                        Stn1 = St1 * Math.Exp(nudt + sigsdt * R1[i, j - 1]);
                        Stn2 = St2 * Math.Exp(nudt + sigsdt * (-R1[i, j - 1]));
                        cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                        cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);
                        St1 = Stn1;
                        St2 = Stn2;
                    }

                    if (isput == false)
                    {
                        a = 0;
                        b = 0;
                        if (St1 > K)
                        {
                            a = rebate;
                        }
                        if (St2 > K)
                        {
                            b = rebate;
                        }
                        CT = 0.5 * (a + beta1 * cv1 + b + beta1 * cv2);
                    }
                    else
                    {

                        a = 0;
                        b = 0;
                        if (St1 < K)
                        {
                            a = rebate;
                        }
                        if (St2 < K)
                        {
                            b = rebate;
                        }
                        CT = 0.5 * (a + beta1 * cv1 + b + beta1 * cv2);
                    }
                    sum_CT = sum_CT + CT;
                    sum_CT2 = sum_CT2 + CT * CT;
                }
                double SD = Math.Sqrt((sum_CT2 - sum_CT * sum_CT / N) * Math.Exp(-2 * r * T) / (N - 1));
                Std = SD / Math.Sqrt(N);
            }

            return Std;
        }
    }

    public class LookbackOption : Options
    {
        public double PriceFixed(int steps, int N, Boolean IsAnti, Boolean IsCV, Boolean IsMT)
        {
            double sum = 0, price = 0;
            double[] S_max1 = new double[N];
            double[] S_min1 = new double[N];
            //We used the simulation matrix we obtained.
            double[,] sims;
            sims = Getsim(steps, N, IsMT);
            for (int i = 0; i < N; i++)
            {
                S_max1[i] = sims[i, 0];
                S_min1[i] = sims[i, 0];
                for (int j = 0; j < steps; j++)
                {
                    if (sims[i, j] > S_max1[i])
                    {
                        S_max1[i] = sims[i, j];
                    }
                    if (sims[i, j] < S_min1[i])
                    {
                        S_min1[i] = sims[i, j];
                    }
                }
            }
            if (IsAnti == false && IsCV == false)
            {
                for (int i = 0; i < N; i++)
                {
                    if (isput == false)
                    {
                        //Gives the option price of call option.
                        sum += Math.Max(S_max1[i] - K, 0);
                    }
                    else
                    {
                        //Gives the option price of put option.
                        sum += Math.Max(K - S_min1[i], 0);
                    }
                }
                price = (sum / N) * Math.Exp(-r * T);
            }
            else if (IsAnti == true && IsCV == false)
            {
                double[] S_max2 = new double[N];
                double[] S_min2 = new double[N];
                double[,] sims2;
                sims2 = Getsim2(steps, N, IsMT);
                for (int i = 0; i < N; i++)
                {
                    S_max2[i] = sims2[i, 0];
                    S_min2[i] = sims2[i, 0];
                    for (int j = 0; j < steps; j++)
                    {
                        if (sims2[i, j] > S_max2[i])
                        {
                            S_max2[i] = sims2[i, j];
                        }
                        if (sims2[i, j] < S_min2[i])
                        {
                            S_min2[i] = sims2[i, j];
                        }
                    }
                }
                for (int i = 0; i < N; i++)
                {
                    if (isput == false)
                    {
                        sum += Math.Max(S_max1[i] - K, 0);
                        sum += Math.Max(S_max2[i] - K, 0);
                    }
                    else
                    {
                        sum += Math.Max(K - S_min1[i], 0);
                        sum += Math.Max(K - S_min2[i], 0);
                    }
                    price = (sum / N / 2) * Math.Exp(-r * T);
                }

            }
            else if(IsAnti == false && IsCV == true)
            {
                double dt = T / steps, beta1 = -1;
                double nudt = (r - sigma * sigma / 2) * dt;
                double sigsdt = sigma * Math.Sqrt(dt);
                double erddt = Math.Exp(r * dt);
                double St_sum;

                double cv, St, Stn;
                double CT, sum_CT = 0, sum_CT2 = 0;
                double[] CT0 = new double[N];
                double t, delta;
                //Parallel.ForEach(IEnum.Step(0, N, 1), new ParallelOptions { MaxDegreeOfParallelism = Thread }, i =>
                for (int i = 0; i < N; i++)
                {
                    St = S0;
                    cv = 0;
                    St_sum = St;
                    for (int j = 1; j <= steps; j++)
                    {
                        t = (j - 1) * dt;
                        delta = BlackScholes.Delta(isput, St, t, K, sigma, T, r);
                        Stn = St * Math.Exp(nudt + sigsdt * R1[i, j - 1]);
                        cv = cv + delta * (Stn - St * erddt);
                        St = Stn;
                        St_sum = St_sum + St;
                    }

                    if (isput == false)
                    {
                        CT = Math.Max(S_max1[i] - K, 0) + beta1 * cv;
                    }
                    else
                    {
                        CT = Math.Max(-S_min1[i] + K, 0) + beta1 * cv;
                    }
                    //sum_CT = sum_CT + CT;
                    //sum_CT2 = sum_CT2 + CT * CT;
                    CT0[i] = CT;
                }
                for (int i = 0; i < N; i++)
                {
                    sum_CT = sum_CT + CT0[i];
                    sum_CT2 = sum_CT2 + CT0[i] * CT0[i];
                }
                price = sum_CT / N * Math.Exp(-r * T);
            }
            else if (IsAnti == true && IsCV == true)
            {
                double[] S_max2 = new double[N];
                double[] S_min2 = new double[N];
                double[,] sims2;
                sims2 = Getsim2(steps, N, IsMT);
                for (int i = 0; i < N; i++)
                {
                    S_max2[i] = sims2[i, 0];
                    S_min2[i] = sims2[i, 0];
                    for (int j = 0; j < steps; j++)
                    {
                        if (sims2[i, j] > S_max2[i])
                        {
                            S_max2[i] = sims2[i, j];
                        }
                        if (sims2[i, j] < S_min2[i])
                        {
                            S_min2[i] = sims2[i, j];
                        }
                    }
                }

                double dt = T / steps, beta1 = -1;
                double nudt = (r - sigma * sigma / 2) * dt;
                double sigsdt = sigma * Math.Sqrt(dt);
                double erddt = Math.Exp(r * dt);

                double sum_CT = 0, sum_CT2 = 0;
                double St1, St2, cv1, cv2, t;
                double delta1, delta2, Stn1, Stn2;
                double CT;

                //Parallel.ForEach(IEnum.Step(0, N, 1), new ParallelOptions { MaxDegreeOfParallelism = Thread }, i =>
                for (int i = 0; i < N; i++)
                {
                    St1 = S0;
                    St2 = S0;
                    cv1 = 0;
                    cv2 = 0;
                    for (int j = 1; j <= steps; j++)
                    {
                        t = (j - 1) * dt;
                        delta1 = BlackScholes.Delta(isput, St1, t, K, sigma, T, r);
                        delta2 = BlackScholes.Delta(isput, St2, t, K, sigma, T, r);
                        Stn1 = St1 * Math.Exp(nudt + sigsdt * R1[i, j - 1]);
                        Stn2 = St2 * Math.Exp(nudt + sigsdt * (-R1[i, j - 1]));
                        cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                        cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);
                        St1 = Stn1;
                        St2 = Stn2;
                    }
                    if (isput == false)
                    {
                        CT = 0.5 * (Math.Max(0, S_max1[i] - K) + beta1 * cv1 + Math.Max(0, S_max2[i] - K) + beta1 * cv2);
                    }
                    else
                    {
                        CT = 0.5 * (Math.Max(0, -S_min1[i] + K) + beta1 * cv1 + Math.Max(0, -S_min2[i] + K) + beta1 * cv2);
                    }
                    sum_CT = sum_CT + CT;
                    sum_CT2 = sum_CT2 + CT * CT;
                }
                price = sum_CT / N * Math.Exp(-r * T);
            }

            return price;
        }
        public double PriceFloating(int steps, int N, Boolean IsAnti, Boolean IsCV, Boolean IsMT)
        {
            double sum = 0, price = 0;
            double[] S_min1 = new double[N];
            //We used the simulation matrix we obtained.
            double[,] sims;
            sims = Getsim(steps, N, IsMT);
            for (int i = 0; i < N; i++)
            {
                S_min1[i] = sims[i, 0];
                for (int j = 0; j < steps; j++)
                {
                    if (sims[i, j] < S_min1[i])
                    {
                        S_min1[i] = sims[i, j];
                    }
                }
            }
            if (IsAnti == false)
            {
                for (int i = 0; i < N; i++)
                {
                    if (isput == false)
                    {
                        //Gives the option price of call option.
                        sum += Math.Max(-S_min1[i] + sims[i, steps], 0);
                    }
                    else
                    {
                        //Gives the option price of put option.
                        sum += Math.Max(S_min1[i] - sims[i, steps], 0);
                    }
                }
                price = (sum / N) * Math.Exp(-r * T);
            }
            else if (IsAnti == true)
            {
                double[] S_min2 = new double[N];
                double[,] sims2;
                sims2 = Getsim2(steps, N, IsMT);
                for (int i = 0; i < N; i++)
                {
                    S_min2[i] = sims2[i, 0];
                    for (int j = 0; j < steps; j++)
                    {
                        if (sims2[i, j] < S_min2[i])
                        {
                            S_min2[i] = sims2[i, j];
                        }
                    }
                }
                for (int i = 0; i < N; i++)
                {
                    if (isput == false)
                    {
                        sum += Math.Max(-S_min1[i] + sims[i, steps], 0);
                        sum += Math.Max(-S_min2[i] + sims2[i, steps], 0);
                    }
                    else
                    {
                        sum += Math.Max(S_min1[i] - sims[i, steps], 0);
                        sum += Math.Max(S_min2[i] - sims2[i, steps], 0);
                    }
                    price = (sum / N / 2) * Math.Exp(-r * T);
                }

            }

            return price;
        }

        public double FixedSE(int steps, int N, Boolean IsAnti, Boolean IsCV, Boolean IsMT)
        {
            double[] S_max1 = new double[N];
            double[] S_min1 = new double[N];
            //We used the simulation matrix we obtained.
            double[,] sims;
            sims = Getsim(steps, N, IsMT);
            for (int i = 0; i < N; i++)
            {
                S_max1[i] = sims[i, 0];
                S_min1[i] = sims[i, 0];
                for (int j = 0; j < steps; j++)
                {
                    if (sims[i, j] > S_max1[i])
                    {
                        S_max1[i] = sims[i, j];
                    }
                    if (sims[i, j] < S_min1[i])
                    {
                        S_min1[i] = sims[i, j];
                    }
                }
            }
            double sums = 0, Std = 0;
            double price = PriceFixed(steps, N, IsAnti, IsCV, IsMT);

            if (IsAnti == false && IsCV == false)
            {
                for (int i = 0; i < N; i++)
                {
                    if (isput == false)
                    {
                        sums += Math.Pow((Math.Max(S_max1[i] - K, 0) * Math.Exp(-r * T) - price), 2);
                    }
                    else
                    {
                        sums += Math.Pow((Math.Max(K - S_min1[i], 0) * Math.Exp(-r * T) - price), 2);
                    }
                }
                Std = Math.Sqrt(sums / (N - 1) / N);
            }
            else if (IsAnti == true && IsCV == false)
            {
                double[] S_max2 = new double[N];
                double[] S_min2 = new double[N];
                double[,] sims2;
                sims2 = Getsim2(steps, N, IsMT);
                for (int i = 0; i < N; i++)
                {
                    S_max2[i] = sims2[i, 0];
                    S_min2[i] = sims2[i, 0];
                    for (int j = 0; j < steps; j++)
                    {
                        if (sims2[i, j] > S_max2[i])
                        {
                            S_max2[i] = sims2[i, j];
                        }
                        if (sims2[i, j] < S_min2[i])
                        {
                            S_min2[i] = sims2[i, j];
                        }
                    }
                }
                for (int i = 0; i < N; i++)
                {
                     if (isput == false)
                     {
                            sums += Math.Pow(((Math.Max(S_max1[i] - K, 0) + Math.Max(S_max2[i] - K, 0)) / 2 * Math.Exp(-r * T) - price), 2);
                      }
                     else
                     {
                            sums += Math.Pow(((Math.Max(K - S_min1[i], 0) + Math.Max(K - S_min2[i], 0)) / 2 * Math.Exp(-r * T) - price), 2);
                      }
                 }
                Std = Math.Sqrt(sums / (N - 1) / N);
                
            }
            else if (IsAnti == false && IsCV == true)
            {
                double dt = T / steps, beta1 = -1;
                double nudt = (r - sigma * sigma / 2) * dt;
                double sigsdt = sigma * Math.Sqrt(dt);
                double erddt = Math.Exp(r * dt);
                double St_sum;

                double cv, St, Stn;
                double CT, sum_CT = 0, sum_CT2 = 0;
                double[] CT0 = new double[N];
                double t, delta;
                //Parallel.ForEach(IEnum.Step(0, N, 1), new ParallelOptions { MaxDegreeOfParallelism = Thread }, i =>
                for (int i = 0; i < N; i++)
                {
                    St = S0;
                    cv = 0;
                    St_sum = St;
                    for (int j = 1; j <= steps; j++)
                    {
                        t = (j - 1) * dt;
                        delta = BlackScholes.Delta(isput, St, t, K, sigma, T, r);
                        Stn = St * Math.Exp(nudt + sigsdt * R1[i, j - 1]);
                        cv = cv + delta * (Stn - St * erddt);
                        St = Stn;
                        St_sum = St_sum + St;
                    }

                    if (isput == false)
                    {
                        CT = Math.Max(S_max1[i] - K, 0) + beta1 * cv;
                    }
                    else
                    {
                        CT = Math.Max(-S_min1[i] + K, 0) + beta1 * cv;
                    }
                    //sum_CT = sum_CT + CT;
                    //sum_CT2 = sum_CT2 + CT * CT;
                    CT0[i] = CT;
                }
                for (int i = 0; i < N; i++)
                {
                    sum_CT = sum_CT + CT0[i];
                    sum_CT2 = sum_CT2 + CT0[i] * CT0[i];
                }
                Std = Math.Sqrt((sum_CT2 - sum_CT * sum_CT / N) * Math.Exp(-2 * r * T) / (N - 1)) / Math.Sqrt(N);
            }
            else if (IsAnti == true && IsCV == true)
            {
                double[] S_max2 = new double[N];
                double[] S_min2 = new double[N];
                double[,] sims2;
                sims2 = Getsim2(steps, N, IsMT);
                for (int i = 0; i < N; i++)
                {
                    S_max2[i] = sims2[i, 0];
                    S_min2[i] = sims2[i, 0];
                    for (int j = 0; j < steps; j++)
                    {
                        if (sims2[i, j] > S_max2[i])
                        {
                            S_max2[i] = sims2[i, j];
                        }
                        if (sims2[i, j] < S_min2[i])
                        {
                            S_min2[i] = sims2[i, j];
                        }
                    }
                }

                double dt = T / steps, beta1 = -1;
                double nudt = (r - sigma * sigma / 2) * dt;
                double sigsdt = sigma * Math.Sqrt(dt);
                double erddt = Math.Exp(r * dt);

                double sum_CT = 0, sum_CT2 = 0;
                double St1, St2, cv1, cv2, t;
                double delta1, delta2, Stn1, Stn2;
                double CT;

                //Parallel.ForEach(IEnum.Step(0, N, 1), new ParallelOptions { MaxDegreeOfParallelism = Thread }, i =>
                for (int i = 0; i < N; i++)
                {
                    St1 = S0;
                    St2 = S0;
                    cv1 = 0;
                    cv2 = 0;
                    for (int j = 1; j <= steps; j++)
                    {
                        t = (j - 1) * dt;
                        delta1 = BlackScholes.Delta(isput, St1, t, K, sigma, T, r);
                        delta2 = BlackScholes.Delta(isput, St2, t, K, sigma, T, r);
                        Stn1 = St1 * Math.Exp(nudt + sigsdt * R1[i, j - 1]);
                        Stn2 = St2 * Math.Exp(nudt + sigsdt * (-R1[i, j - 1]));
                        cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                        cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);
                        St1 = Stn1;
                        St2 = Stn2;
                    }
                    if (isput == false)
                    {
                        CT = 0.5 * (Math.Max(0, S_max1[i] - K) + beta1 * cv1 + Math.Max(0, S_max2[i] - K) + beta1 * cv2);
                    }
                    else
                    {
                        CT = 0.5 * (Math.Max(0, -S_min1[i] + K) + beta1 * cv1 + Math.Max(0, -S_min2[i] + K) + beta1 * cv2);
                    }
                    sum_CT = sum_CT + CT;
                    sum_CT2 = sum_CT2 + CT * CT;
                }
                double SD = Math.Sqrt((sum_CT2 - sum_CT * sum_CT / N) * Math.Exp(-2 * r * T) / (N - 1));
                Std = SD / Math.Sqrt(N);
            }

            return Std;
        }
        public double FloatingSE(int steps, int N, Boolean IsAnti, Boolean IsCV, Boolean IsMT)
        {
            double[] S_min1 = new double[N];
            //We used the simulation matrix we obtained.
            double[,] sims;
            sims = Getsim(steps, N, IsMT);
            for (int i = 0; i < N; i++)
            {
                S_min1[i] = sims[i, 0];
                for (int j = 0; j < steps; j++)
                {
                    if (sims[i, j] < S_min1[i])
                    {
                        S_min1[i] = sims[i, j];
                    }
                }
            }
            double sums = 0, Std = 0;
            double price = PriceFloating(steps, N, IsAnti, IsCV, IsMT);

            if (IsAnti == false && IsCV == false)
            {
                for (int i = 0; i < N; i++)
                {
                    if (isput == false)
                    {
                        sums += Math.Pow((Math.Max(-S_min1[i] + sims[i, steps], 0) * Math.Exp(-r * T) - price), 2);
                    }
                    else
                    {
                        sums += Math.Pow((Math.Max(S_min1[i] - sims[i, steps], 0) * Math.Exp(-r * T) - price), 2);
                    }
                }
                Std = Math.Sqrt(sums / (N - 1) / N);
            }
            else if (IsAnti == true && IsCV == false)
            {
                double[] S_min2 = new double[N];
                double[,] sims2;
                sims2 = Getsim2(steps, N, IsMT);
                for (int i = 0; i < N; i++)
                {
                    S_min2[i] = sims2[i, 0];
                    for (int j = 0; j < steps; j++)
                    {
                        if (sims2[i, j] < S_min2[i])
                        {
                            S_min2[i] = sims2[i, j];
                        }
                    }
                }
                for (int i = 0; i < N; i++)
                {
                    if (isput == false)
                    {
                        sums += Math.Pow(((Math.Max(-S_min1[i] + sims[i, steps], 0) + Math.Max(-S_min2[i] + sims2[i, steps], 0)) / 2 * Math.Exp(-r * T) - price), 2);
                    }
                    else
                    {
                        sums += Math.Pow(((Math.Max(S_min1[i] - sims[i, steps], 0) + Math.Max(S_min2[i] - sims2[i, steps], 0)) / 2 * Math.Exp(-r * T) - price), 2);
                    }
                }
                Std = Math.Sqrt(sums / (N - 1) / N);

            }
            else if (IsAnti == false && IsCV == true)
            {
                double dt = T / steps, beta1 = -1;
                double nudt = (r - sigma * sigma / 2) * dt;
                double sigsdt = sigma * Math.Sqrt(dt);
                double erddt = Math.Exp(r * dt);
                double St_sum;

                double cv, St, Stn;
                double CT, sum_CT = 0, sum_CT2 = 0;
                double[] CT0 = new double[N];
                double t, delta;
                //Parallel.ForEach(IEnum.Step(0, N, 1), new ParallelOptions { MaxDegreeOfParallelism = Thread }, i =>
                for (int i = 0; i < N; i++)
                {
                    St = S0;
                    cv = 0;
                    St_sum = St;
                    for (int j = 1; j <= steps; j++)
                    {
                        t = (j - 1) * dt;
                        delta = BlackScholes.Delta(isput, St, t, K, sigma, T, r);
                        Stn = St * Math.Exp(nudt + sigsdt * R1[i, j - 1]);
                        cv = cv + delta * (Stn - St * erddt);
                        St = Stn;
                        St_sum = St_sum + St;
                    }

                    if (isput == false)
                    {
                        CT = Math.Max(-S_min1[i] + sims[i, steps], 0) + beta1 * cv;
                    }
                    else
                    {
                        CT = Math.Max(S_min1[i] - sims[i, steps], 0) + beta1 * cv;
                    }
                    //sum_CT = sum_CT + CT;
                    //sum_CT2 = sum_CT2 + CT * CT;
                    CT0[i] = CT;
                }
                for (int i = 0; i < N; i++)
                {
                    sum_CT = sum_CT + CT0[i];
                    sum_CT2 = sum_CT2 + CT0[i] * CT0[i];
                }
                Std = Math.Sqrt((sum_CT2 - sum_CT * sum_CT / N) * Math.Exp(-2 * r * T) / (N - 1)) / Math.Sqrt(N);
            }
            else if (IsAnti == true && IsCV == true)
            {
                double[] S_min2 = new double[N];
                double[,] sims2;
                sims2 = Getsim2(steps, N, IsMT);
                for (int i = 0; i < N; i++)
                {
                    S_min2[i] = sims2[i, 0];
                    for (int j = 0; j < steps; j++)
                    {
                        if (sims2[i, j] < S_min2[i])
                        {
                            S_min2[i] = sims2[i, j];
                        }
                    }
                }

                double dt = T / steps, beta1 = -1;
                double nudt = (r - sigma * sigma / 2) * dt;
                double sigsdt = sigma * Math.Sqrt(dt);
                double erddt = Math.Exp(r * dt);

                double sum_CT = 0, sum_CT2 = 0;
                double St1, St2, cv1, cv2, t;
                double delta1, delta2, Stn1, Stn2;
                double CT;

                //Parallel.ForEach(IEnum.Step(0, N, 1), new ParallelOptions { MaxDegreeOfParallelism = Thread }, i =>
                for (int i = 0; i < N; i++)
                {
                    St1 = S0;
                    St2 = S0;
                    cv1 = 0;
                    cv2 = 0;
                    for (int j = 1; j <= steps; j++)
                    {
                        t = (j - 1) * dt;
                        delta1 = BlackScholes.Delta(isput, St1, t, K, sigma, T, r);
                        delta2 = BlackScholes.Delta(isput, St2, t, K, sigma, T, r);
                        Stn1 = St1 * Math.Exp(nudt + sigsdt * R1[i, j - 1]);
                        Stn2 = St2 * Math.Exp(nudt + sigsdt * (-R1[i, j - 1]));
                        cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                        cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);
                        St1 = Stn1;
                        St2 = Stn2;
                    }
                    if (isput == false)
                    {
                        CT = 0.5 * (Math.Max(0, -S_min1[i] + sims[i, steps]) + beta1 * cv1 + Math.Max(0, -S_min2[i] + sims2[i, steps]) + beta1 * cv2);
                    }
                    else
                    {
                        CT = 0.5 * (Math.Max(0, S_min1[i] - sims[i, steps]) + beta1 * cv1 + Math.Max(0, S_min2[i] - sims2[i, steps]) + beta1 * cv2);
                    }
                    sum_CT = sum_CT + CT;
                    sum_CT2 = sum_CT2 + CT * CT;
                }
                double SD = Math.Sqrt((sum_CT2 - sum_CT * sum_CT / N) * Math.Exp(-2 * r * T) / (N - 1));
                Std = SD / Math.Sqrt(N);
            }

            return Std;
        }
    }

    public class BarrierOption : Options
    {
        public double barrier;

        public double PriceIn(int steps, int N, Boolean IsAnti, Boolean IsCV, Boolean IsMT)
        {
            double sum = 0, price = 0;
            double a, b;
            //We used the simulation matrix we obtained.
            double[,] sims;
            sims = Getsim(steps, N, IsMT);
            double[,] i1 = new double[N, steps+1];
            double[] i_trails1 = new double[N];
            if (S0 > barrier)
            {
                for(int i = 0; i < N; i++)
                {
                    i_trails1[i] = 1;
                    for(int j = 0; j <= steps; j++)
                    {
                        i1[i, j] = 1;
                        if (sims[i, j] < barrier)
                        {
                            i1[i, j] = 0;
                        }
                        i_trails1[i] = i_trails1[i] * i1[i, j];
                    }
                }
            }
            else
            {
                for (int i = 0; i < N; i++)
                {
                    i_trails1[i] = 1;
                    for (int j = 0; j <= steps; j++)
                    {
                        i1[i, j] = 1;
                        if (sims[i, j] > barrier)
                        {
                            i1[i, j] = 0;
                        }
                        i_trails1[i] = i_trails1[i] * i1[i, j];
                    }
                }
            }
            if (IsAnti == false && IsCV == false)
            {
                for (int i = 0; i < N; i++)
                {
                    if (isput == false)
                    {
                        if (i_trails1[i] == 0)
                        {
                            sum += Math.Max(sims[i, steps] - K, 0);
                        }
                        else
                        {
                            sum += 0;
                        }
                    }
                    else
                    {
                        if(i_trails1[i] == 0)
                        {
                            sum += Math.Max(K - sims[i, steps], 0);
                        }
                        else
                        {
                            sum += 0;
                        }
                    }
                }
                price = (sum / N) * Math.Exp(-r * T);
            }
            else if (IsAnti == true && IsCV == false)
            {
                double[,] sims2;
                sims2 = Getsim2(steps, N, IsMT);
                double[,] i2 = new double[N, steps + 1];
                double[] i_trails2 = new double[N];

                if (S0 > barrier)
                {
                    for (int i = 0; i < N; i++)
                    {
                        i_trails2[i] = 1;
                        for (int j = 0; j <= steps; j++)
                        {
                            i2[i, j] = 1;
                            if (sims2[i, j] < barrier)
                            {
                                i2[i, j] = 0;
                            }
                            i_trails2[i] = i_trails2[i] * i2[i, j];
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < N; i++)
                    {
                        i_trails2[i] = 1;
                        for (int j = 0; j <= steps; j++)
                        {
                            i2[i, j] = 1;
                            if (sims2[i, j] > barrier)
                            {
                                i2[i, j] = 0;
                            }
                            i_trails2[i] = i_trails2[i] * i2[i, j];
                        }
                    }
                }

                for (int i = 0; i < N; i++)
                {
                    if (isput == false)
                    {
                        if (i_trails1[i] == 0)
                        {
                            sum += Math.Max(sims[i, steps] - K, 0);
                        }
                        if (i_trails2[i] == 0)
                        {
                            sum += Math.Max(sims2[i, steps] - K, 0);
                        }
                    }
                    else
                    {
                        if (i_trails1[i] == 0)
                        {
                            sum += Math.Max(K - sims[i, steps], 0);
                        }
                        if (i_trails2[i] == 0)
                        {
                            sum += Math.Max(K - sims2[i, steps], 0);
                        }
                    }
                    price = (sum / N / 2) * Math.Exp(-r * T);
                }
            }
            else if (IsAnti==false && IsCV == true)
            {
                double dt = T / steps, beta1 = -1;
                double nudt = (r - sigma * sigma / 2) * dt;
                double sigsdt = sigma * Math.Sqrt(dt);
                double erddt = Math.Exp(r * dt);

                double cv, St, Stn;
                double CT, sum_CT = 0, sum_CT2 = 0;
                double[] CT0 = new double[N];
                double t, delta;
                //Parallel.ForEach(IEnum.Step(0, N, 1), new ParallelOptions { MaxDegreeOfParallelism = Thread }, i =>
                for (int i = 0; i < N; i++)
                {
                    St = S0;
                    cv = 0;
                    for (int j = 1; j <= steps; j++)
                    {
                        t = (j - 1) * dt;
                        delta = BlackScholes.Delta(isput, St, t, K, sigma, T, r);
                        Stn = St * Math.Exp(nudt + sigsdt * R1[i, j - 1]);
                        cv = cv + delta * (Stn - St * erddt);
                        St = Stn;
                    }
                    CT = 0;
                    if (isput == false)
                    {
                        if (i_trails1[i] == 0)
                        {
                            CT = Math.Max(St - K, 0) + beta1 * cv;
                        }
                    }
                    else
                    {
                        if (i_trails1[i] == 0)
                        {
                            CT = Math.Max(-St + K, 0) + beta1 * cv;
                        }
                    }
                    CT0[i] = CT;
                }
                for (int i = 0; i < N; i++)
                {
                    sum_CT = sum_CT + CT0[i];
                    sum_CT2 = sum_CT2 + CT0[i] * CT0[i];
                }
                price = sum_CT / N * Math.Exp(-r * T);
            }
            else if (IsAnti == true && IsCV == true)
            {
                double[,] sims2;
                sims2 = Getsim2(steps, N, IsMT);
                double[,] i2 = new double[N, steps + 1];
                double[] i_trails2 = new double[N];
                if (S0 > barrier)
                {
                    for (int i = 0; i < N; i++)
                    {
                        i_trails2[i] = 1;
                        for (int j = 0; j <= steps; j++)
                        {
                            i2[i, j] = 1;
                            if (sims2[i, j] < barrier)
                            {
                                i2[i, j] = 0;
                            }
                            i_trails2[i] = i_trails2[i] * i2[i, j];
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < N; i++)
                    {
                        i_trails2[i] = 1;
                        for (int j = 0; j <= steps; j++)
                        {
                            i2[i, j] = 1;
                            if (sims2[i, j] > barrier)
                            {
                                i2[i, j] = 0;
                            }
                            i_trails2[i] = i_trails2[i] * i2[i, j];
                        }
                    }
                }
                double dt = T / steps, beta1 = -1;
                double nudt = (r - sigma * sigma / 2) * dt;
                double sigsdt = sigma * Math.Sqrt(dt);
                double erddt = Math.Exp(r * dt);

                double sum_CT = 0, sum_CT2 = 0;
                double St1, St2, cv1, cv2, t;
                double delta1, delta2, Stn1, Stn2;
                double CT;


                //Parallel.ForEach(IEnum.Step(0, N, 1), new ParallelOptions { MaxDegreeOfParallelism = Thread }, i =>
                for (int i = 0; i < N; i++)
                {
                    St1 = S0;
                    St2 = S0;
                    cv1 = 0;
                    cv2 = 0;
                    for (int j = 1; j <= steps; j++)
                    {
                        t = (j - 1) * dt;
                        delta1 = BlackScholes.Delta(isput, St1, t, K, sigma, T, r);
                        delta2 = BlackScholes.Delta(isput, St2, t, K, sigma, T, r);
                        Stn1 = St1 * Math.Exp(nudt + sigsdt * R1[i, j - 1]);
                        Stn2 = St2 * Math.Exp(nudt + sigsdt * (-R1[i, j - 1]));
                        cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                        cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);
                        St1 = Stn1;
                        St2 = Stn2;
                    }
                    a = 0;
                    b = 0;
                    CT = 0;
                    if (isput == false)
                    {
                        if (i_trails1[i] == 0)
                        {
                            a = St1 - K;
                            CT = CT + 0.5 * (Math.Max(0, a) + beta1 * cv1);
                        }
                        if (i_trails2[i] == 0)
                        {
                            b = St2 - K;
                            CT = CT + 0.5 * (Math.Max(0, b) + beta1 * cv2);
                        }
                    }
                    else
                    {
                        if (i_trails1[i] == 0)
                        {
                            a = -St1 + K;
                            CT = CT + 0.5 * (Math.Max(0, a) + beta1 * cv1);
                        }
                        if (i_trails2[i] == 0)
                        {
                            b = -St2 + K;
                            CT = CT + 0.5 * (Math.Max(0, b) + beta1 * cv2);
                        }
                    }
                    sum_CT = sum_CT + CT;
                    sum_CT2 = sum_CT2 + CT * CT;
                }
                price = sum_CT / N * Math.Exp(-r * T);
            }

            return price;
        }
        public double PriceOut(int steps, int N, Boolean IsAnti, Boolean IsCV, Boolean IsMT)
        {
            double sum = 0, price = 0;
            double a, b;
            //We used the simulation matrix we obtained.
            double[,] sims;
            sims = Getsim(steps, N, IsMT);
            double[,] i1 = new double[N, steps + 1];
            double[] i_trails1 = new double[N];
            if (S0 > barrier)
            {
                for (int i = 0; i < N; i++)
                {
                    i_trails1[i] = 1;
                    for (int j = 0; j <= steps; j++)
                    {
                        i1[i, j] = 1;
                        if (sims[i, j] < barrier)
                        {
                            i1[i, j] = 0;
                        }
                        i_trails1[i] = i_trails1[i] * i1[i, j];
                    }
                }
            }
            else
            {
                for (int i = 0; i < N; i++)
                {
                    i_trails1[i] = 1;
                    for (int j = 0; j <= steps; j++)
                    {
                        i1[i, j] = 1;
                        if (sims[i, j] > barrier)
                        {
                            i1[i, j] = 0;
                        }
                        i_trails1[i] = i_trails1[i] * i1[i, j];
                    }
                }
            }
            if (IsAnti == false && IsCV == false)
            {
                for (int i = 0; i < N; i++)
                {
                    if (isput == false)
                    {
                        if (i_trails1[i] == 1)
                        {
                            sum += Math.Max(sims[i, steps] - K, 0);
                        }
                    }
                    else
                    {
                        if (i_trails1[i] == 1)
                        {
                            sum += Math.Max(K - sims[i, steps], 0);
                        }
                    }
                }
                price = (sum / N) * Math.Exp(-r * T);
            }
            else if (IsAnti == true && IsCV == false)
            {
                double[,] sims2;
                sims2 = Getsim2(steps, N, IsMT);
                double[,] i2 = new double[N, steps + 1];
                double[] i_trails2 = new double[N];

                if (S0 > barrier)
                {
                    for (int i = 0; i < N; i++)
                    {
                        i_trails2[i] = 1;
                        for (int j = 0; j <= steps; j++)
                        {
                            i2[i, j] = 1;
                            if (sims2[i, j] < barrier)
                            {
                                i2[i, j] = 0;
                            }
                            i_trails2[i] = i_trails2[i] * i2[i, j];
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < N; i++)
                    {
                        i_trails2[i] = 1;
                        for (int j = 0; j <= steps; j++)
                        {
                            i2[i, j] = 1;
                            if (sims2[i, j] > barrier)
                            {
                                i2[i, j] = 0;
                            }
                            i_trails2[i] = i_trails2[i] * i2[i, j];
                        }
                    }
                }

                for (int i = 0; i < N; i++)
                {
                    if (isput == false)
                    {
                        if (i_trails1[i] == 1)
                        {
                            sum += Math.Max(sims[i, steps] - K, 0);
                        }
                        if (i_trails2[i] == 1)
                        {
                            sum += Math.Max(sims2[i, steps] - K, 0);
                        }
                    }
                    else
                    {
                        if (i_trails1[i] == 1)
                        {
                            sum += Math.Max(K - sims[i, steps], 0);
                        }
                        if (i_trails2[i] == 1)
                        {
                            sum += Math.Max(K - sims2[i, steps], 0);
                        }
                    }
                    price = (sum / N / 2) * Math.Exp(-r * T);
                }
            }
            else if (IsAnti ==false && IsCV == true)
            {
                double dt = T / steps, beta1 = -1;
                double nudt = (r - sigma * sigma / 2) * dt;
                double sigsdt = sigma * Math.Sqrt(dt);
                double erddt = Math.Exp(r * dt);

                double cv, St, Stn;
                double CT, sum_CT = 0, sum_CT2 = 0;
                double[] CT0 = new double[N];
                double t, delta;
                //Parallel.ForEach(IEnum.Step(0, N, 1), new ParallelOptions { MaxDegreeOfParallelism = Thread }, i =>
                for (int i = 0; i < N; i++)
                {
                    St = S0;
                    cv = 0;
                    for (int j = 1; j <= steps; j++)
                    {
                        t = (j - 1) * dt;
                        delta = BlackScholes.Delta(isput, St, t, K, sigma, T, r);
                        Stn = St * Math.Exp(nudt + sigsdt * R1[i, j - 1]);
                        cv = cv + delta * (Stn - St * erddt);
                        St = Stn;
                    }
                    CT = 0;
                    if (isput == false)
                    {
                        if (i_trails1[i] == 1)
                        {
                            CT = Math.Max(St - K, 0) + beta1 * cv;
                        }
                    }
                    else
                    {
                        if (i_trails1[i] == 1)
                        {
                            CT = Math.Max(-St + K, 0) + beta1 * cv;
                        }
                    }
                    CT0[i] = CT;
                }
                for (int i = 0; i < N; i++)
                {
                    sum_CT = sum_CT + CT0[i];
                    sum_CT2 = sum_CT2 + CT0[i] * CT0[i];
                }
                price = sum_CT / N * Math.Exp(-r * T);
            }
            else if (IsAnti == true && IsCV == true)
            {
                double[,] sims2;
                sims2 = Getsim2(steps, N, IsMT);
                double[,] i2 = new double[N, steps + 1];
                double[] i_trails2 = new double[N];
                if (S0 > barrier)
                {
                    for (int i = 0; i < N; i++)
                    {
                        i_trails2[i] = 1;
                        for (int j = 0; j <= steps; j++)
                        {
                            i2[i, j] = 1;
                            if (sims2[i, j] < barrier)
                            {
                                i2[i, j] = 0;
                            }
                            i_trails2[i] = i_trails2[i] * i2[i, j];
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < N; i++)
                    {
                        i_trails2[i] = 1;
                        for (int j = 0; j <= steps; j++)
                        {
                            i2[i, j] = 1;
                            if (sims2[i, j] > barrier)
                            {
                                i2[i, j] = 0;
                            }
                            i_trails2[i] = i_trails2[i] * i2[i, j];
                        }
                    }
                }
                double dt = T / steps, beta1 = -1;
                double nudt = (r - sigma * sigma / 2) * dt;
                double sigsdt = sigma * Math.Sqrt(dt);
                double erddt = Math.Exp(r * dt);

                double sum_CT = 0, sum_CT2 = 0;
                double St1, St2, cv1, cv2, t;
                double delta1, delta2, Stn1, Stn2;
                double CT;


                //Parallel.ForEach(IEnum.Step(0, N, 1), new ParallelOptions { MaxDegreeOfParallelism = Thread }, i =>
                for (int i = 0; i < N; i++)
                {
                    St1 = S0;
                    St2 = S0;
                    cv1 = 0;
                    cv2 = 0;
                    for (int j = 1; j <= steps; j++)
                    {
                        t = (j - 1) * dt;
                        delta1 = BlackScholes.Delta(isput, St1, t, K, sigma, T, r);
                        delta2 = BlackScholes.Delta(isput, St2, t, K, sigma, T, r);
                        Stn1 = St1 * Math.Exp(nudt + sigsdt * R1[i, j - 1]);
                        Stn2 = St2 * Math.Exp(nudt + sigsdt * (-R1[i, j - 1]));
                        cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                        cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);
                        St1 = Stn1;
                        St2 = Stn2;
                    }
                    a = 0;
                    b = 0;
                    CT = 0;
                    if (isput == false)
                    {
                        if (i_trails1[i] == 1)
                        {
                            a = St1 - K;
                            CT = CT + 0.5 * (Math.Max(0, a) + beta1 * cv1);
                        }
                        if (i_trails2[i] == 1)
                        {
                            b = St2 - K;
                            CT = CT + 0.5 * (Math.Max(0, b) + beta1 * cv2);
                        }
                    }
                    else
                    {
                        if (i_trails1[i] == 1)
                        {
                            a = -St1 + K;
                            CT = CT + 0.5 * (Math.Max(0, a) + beta1 * cv1);
                        }
                        if (i_trails2[i] == 1)
                        {
                            b = -St2 + K;
                            CT = CT + 0.5 * (Math.Max(0, b) + beta1 * cv2);
                        }
                    }
                    sum_CT = sum_CT + CT;
                    sum_CT2 = sum_CT2 + CT * CT;
                }
                price = sum_CT / N * Math.Exp(-r * T);
            }

            return price;
        }

        public double InSE(int steps, int N, Boolean IsAnti, Boolean IsCV, Boolean IsMT)
        {
            double[,] sims;
            sims = Getsim(steps, N, IsMT);
            double sums = 0, Std = 0;
            double price = PriceIn(steps, N, IsAnti, IsCV, IsMT);
            double[,] i1 = new double[N, steps + 1];
            double[] i_trails1 = new double[N];
            double a, b;
            if (S0 > barrier)
            {
                for (int i = 0; i < N; i++)
                {
                    i_trails1[i] = 1;
                    for (int j = 0; j <= steps; j++)
                    {
                        i1[i, j] = 1;
                        if (sims[i, j] < barrier)
                        {
                            i1[i, j] = 0;
                        }
                        i_trails1[i] = i_trails1[i] * i1[i, j];
                    }
                }
            }
            else
            {
                for (int i = 0; i < N; i++)
                {
                    i_trails1[i] = 1;
                    for (int j = 0; j <= steps; j++)
                    {
                        i1[i, j] = 1;
                        if (sims[i, j] > barrier)
                        {
                            i1[i, j] = 0;
                        }
                        i_trails1[i] = i_trails1[i] * i1[i, j];
                    }
                }
            }
            if (IsAnti == false && IsCV == false)
            {
                for (int i = 0; i < N; i++)
                {
                    if (isput == false)
                    {
                        if (i_trails1[i] == 0)
                        {
                            sums += Math.Pow((Math.Max(sims[i, steps] - K, 0) * Math.Exp(-r * T) - price), 2);
                        }
                    }
                    else
                    {
                        if (i_trails1[i] == 0)
                        {
                            sums += Math.Pow((Math.Max(K - sims[i, steps], 0) * Math.Exp(-r * T) - price), 2);
                        }
                    }
                }
                Std = Math.Sqrt(sums / (N - 1) / N);
            }
            else if (IsAnti == true && IsCV == false)
            {
                double[,] sims2;
                sims2 = Getsim2(steps, N, IsMT);
                double[,] i2 = new double[N, steps + 1];
                double[] i_trails2 = new double[N];
                if (S0 > barrier)
                {
                    for (int i = 0; i < N; i++)
                    {
                        i_trails2[i] = 1;
                        for (int j = 0; j <= steps; j++)
                        {
                            i2[i, j] = 1;
                            if (sims2[i, j] < barrier)
                            {
                                i2[i, j] = 0;
                            }
                            i_trails2[i] = i_trails2[i] * i2[i, j];
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < N; i++)
                    {
                        i_trails2[i] = 1;
                        for (int j = 0; j <= steps; j++)
                        {
                            i2[i, j] = 1;
                            if (sims2[i, j] > barrier)
                            {
                                i2[i, j] = 0;
                            }
                            i_trails2[i] = i_trails2[i] * i2[i, j];
                        }
                    }
                }
                for (int i = 0; i < N; i++)
                {
                    a = 0;
                    b = 0;
                    if (isput == false)
                    {
                        if (i_trails1[i] == 0)
                        {
                            a = sims[i, steps] - K;
                        }
                        if (i_trails2[i] == 0)
                        {
                            b = sims2[i, steps] - K;
                        }
                        sums += Math.Pow(((Math.Max(a, 0) + Math.Max(b, 0)) / 2 * Math.Exp(-r * T) - price), 2);
                    }
                    else
                    {
                        if (i_trails1[i] == 0)
                        {
                            a = K - sims[i, steps];
                        }
                        if (i_trails2[i] == 0)
                        {
                            b = K - sims2[i, steps];
                        }
                        sums += Math.Pow(((Math.Max(a, 0) + Math.Max(b, 0)) / 2 * Math.Exp(-r * T) - price), 2);
                    }
                    Std = Math.Sqrt(sums / (N - 1) / N);
                }

            }
            else if (IsAnti == false && IsCV == true)
            {
                double dt = T / steps, beta1 = -1;
                double nudt = (r - sigma * sigma / 2) * dt;
                double sigsdt = sigma * Math.Sqrt(dt);
                double erddt = Math.Exp(r * dt);

                double cv, St, Stn;
                double CT, sum_CT = 0, sum_CT2 = 0;
                double[] CT0 = new double[N];
                double t, delta;
                //Parallel.ForEach(IEnum.Step(0, N, 1), new ParallelOptions { MaxDegreeOfParallelism = Thread }, i =>
                for (int i = 0; i < N; i++)
                {
                    St = S0;
                    cv = 0;
                    for (int j = 1; j <= steps; j++)
                    {
                        t = (j - 1) * dt;
                        delta = BlackScholes.Delta(isput, St, t, K, sigma, T, r);
                        Stn = St * Math.Exp(nudt + sigsdt * R1[i, j - 1]);
                        cv = cv + delta * (Stn - St * erddt);
                        St = Stn;
                    }
                    CT = 0;
                    if (isput == false)
                    {
                        if (i_trails1[i] == 0)
                        {
                            CT = Math.Max(St - K, 0) + beta1 * cv;
                        }
                    }
                    else
                    {
                        if (i_trails1[i] == 0)
                        {
                            CT = Math.Max(-St + K, 0) + beta1 * cv;
                        }
                    }
                    CT0[i] = CT;
                }
                for (int i = 0; i < N; i++)
                {
                    sum_CT = sum_CT + CT0[i];
                    sum_CT2 = sum_CT2 + CT0[i] * CT0[i];
                }
                Std = Math.Sqrt((sum_CT2 - sum_CT * sum_CT / N) * Math.Exp(-2 * r * T) / (N - 1)) / Math.Sqrt(N);
            }
            else if (IsAnti == true && IsCV == true)
            {
                double[,] sims2;
                sims2 = Getsim2(steps, N, IsMT);
                double[,] i2 = new double[N, steps + 1];
                double[] i_trails2 = new double[N];
                if (S0 > barrier)
                {
                    for (int i = 0; i < N; i++)
                    {
                        i_trails2[i] = 1;
                        for (int j = 0; j <= steps; j++)
                        {
                            i2[i, j] = 1;
                            if (sims2[i, j] < barrier)
                            {
                                i2[i, j] = 0;
                            }
                            i_trails2[i] = i_trails2[i] * i2[i, j];
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < N; i++)
                    {
                        i_trails2[i] = 1;
                        for (int j = 0; j <= steps; j++)
                        {
                            i2[i, j] = 1;
                            if (sims2[i, j] > barrier)
                            {
                                i2[i, j] = 0;
                            }
                            i_trails2[i] = i_trails2[i] * i2[i, j];
                        }
                    }
                }
                double dt = T / steps, beta1 = -1;
                double nudt = (r - sigma * sigma / 2) * dt;
                double sigsdt = sigma * Math.Sqrt(dt);
                double erddt = Math.Exp(r * dt);

                double sum_CT = 0, sum_CT2 = 0;
                double St1, St2, cv1, cv2, t;
                double delta1, delta2, Stn1, Stn2;
                double CT;


                //Parallel.ForEach(IEnum.Step(0, N, 1), new ParallelOptions { MaxDegreeOfParallelism = Thread }, i =>
                for (int i = 0; i < N; i++)
                {
                    St1 = S0;
                    St2 = S0;
                    cv1 = 0;
                    cv2 = 0;
                    for (int j = 1; j <= steps; j++)
                    {
                        t = (j - 1) * dt;
                        delta1 = BlackScholes.Delta(isput, St1, t, K, sigma, T, r);
                        delta2 = BlackScholes.Delta(isput, St2, t, K, sigma, T, r);
                        Stn1 = St1 * Math.Exp(nudt + sigsdt * R1[i, j - 1]);
                        Stn2 = St2 * Math.Exp(nudt + sigsdt * (-R1[i, j - 1]));
                        cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                        cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);
                        St1 = Stn1;
                        St2 = Stn2;
                    }
                    a = 0;
                    b = 0;
                    CT = 0;
                    if (isput == false)
                    {
                        if(i_trails1[i] == 0)
                        {
                            a = St1 - K;
                            CT = CT + 0.5 * (Math.Max(0, a) + beta1 * cv1);
                        }
                        if (i_trails2[i] == 0)
                        {
                            b = St2 - K;
                            CT = CT + 0.5 * (Math.Max(0, b) + beta1 * cv2);
                        }
                    }
                    else
                    {
                        if (i_trails1[i] == 0)
                        {
                            a = -St1 + K;
                            CT = CT + 0.5 * (Math.Max(0, a) + beta1 * cv1);
                        }
                        if (i_trails2[i] == 0)
                        {
                            b = -St2 + K;
                            CT = CT + 0.5 * (Math.Max(0, b) + beta1 * cv2);
                        }
                    }
                    sum_CT = sum_CT + CT;
                    sum_CT2 = sum_CT2 + CT * CT;
                }
                double SD = Math.Sqrt((sum_CT2 - sum_CT * sum_CT / N) * Math.Exp(-2 * r * T) / (N - 1));
                Std = SD / Math.Sqrt(N);
            }

                return Std;
        }
        public double OutSE(int steps, int N, Boolean IsAnti, Boolean IsCV, Boolean IsMT)
        {
            double[,] sims;
            sims = Getsim(steps, N, IsMT);
            double sums = 0, Std = 0;
            double price = PriceIn(steps, N, IsAnti, IsCV, IsMT);
            double[,] i1 = new double[N, steps + 1];
            double[] i_trails1 = new double[N];
            double a, b;
            if (S0 > barrier)
            {
                for (int i = 0; i < N; i++)
                {
                    i_trails1[i] = 1;
                    for (int j = 0; j <= steps; j++)
                    {
                        i1[i, j] = 1;
                        if (sims[i, j] < barrier)
                        {
                            i1[i, j] = 0;
                        }
                        i_trails1[i] = i_trails1[i] * i1[i, j];
                    }
                }
            }
            else
            {
                for (int i = 0; i < N; i++)
                {
                    i_trails1[i] = 1;
                    for (int j = 0; j <= steps; j++)
                    {
                        i1[i, j] = 1;
                        if (sims[i, j] > barrier)
                        {
                            i1[i, j] = 0;
                        }
                        i_trails1[i] = i_trails1[i] * i1[i, j];
                    }
                }
            }
            if (IsAnti == false && IsCV == false)
            {
                for (int i = 0; i < N; i++)
                {
                    if (isput == false)
                    {
                        if (i_trails1[i] == 1)
                        {
                            sums += Math.Pow((Math.Max(sims[i, steps] - K, 0) * Math.Exp(-r * T) - price), 2);
                        }
                    }
                    else
                    {
                        if (i_trails1[i] == 1)
                        {
                            sums += Math.Pow((Math.Max(K - sims[i, steps], 0) * Math.Exp(-r * T) - price), 2);
                        }
                    }
                }
                Std = Math.Sqrt(sums / (N - 1) / N);
            }
            else if (IsAnti == true && IsCV == false)
            {
                double[,] sims2;
                sims2 = Getsim2(steps, N, IsMT);
                double[,] i2 = new double[N, steps + 1];
                double[] i_trails2 = new double[N];
                if (S0 > barrier)
                {
                    for (int i = 0; i < N; i++)
                    {
                        i_trails2[i] = 1;
                        for (int j = 0; j <= steps; j++)
                        {
                            i2[i, j] = 1;
                            if (sims2[i, j] < barrier)
                            {
                                i2[i, j] = 0;
                            }
                            i_trails2[i] = i_trails2[i] * i2[i, j];
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < N; i++)
                    {
                        i_trails2[i] = 1;
                        for (int j = 0; j <= steps; j++)
                        {
                            i2[i, j] = 1;
                            if (sims2[i, j] > barrier)
                            {
                                i2[i, j] = 0;
                            }
                            i_trails2[i] = i_trails2[i] * i2[i, j];
                        }
                    }
                }
                for (int i = 0; i < N; i++)
                {
                    a = 0;
                    b = 0;
                    if (isput == false)
                    {
                        if (i_trails1[i] == 1)
                        {
                            a = sims[i, steps] - K;
                        }
                        if (i_trails2[i] == 1)
                        {
                            b = sims2[i, steps] - K;
                        }
                        sums += Math.Pow(((Math.Max(a, 0) + Math.Max(b, 0)) / 2 * Math.Exp(-r * T) - price), 2);
                    }
                    else
                    {
                        if (i_trails1[i] == 1)
                        {
                            a = K - sims[i, steps];
                        }
                        if (i_trails2[i] == 1)
                        {
                            b = K - sims2[i, steps];
                        }
                        sums += Math.Pow(((Math.Max(a, 0) + Math.Max(b, 0)) / 2 * Math.Exp(-r * T) - price), 2);
                    }
                    Std = Math.Sqrt(sums / (N - 1) / N);
                }

            }
            else if (IsAnti == false && IsCV == true)
            {
                double dt = T / steps, beta1 = -1;
                double nudt = (r - sigma * sigma / 2) * dt;
                double sigsdt = sigma * Math.Sqrt(dt);
                double erddt = Math.Exp(r * dt);

                double cv, St, Stn;
                double CT, sum_CT = 0, sum_CT2 = 0;
                double[] CT0 = new double[N];
                double t, delta;
                //Parallel.ForEach(IEnum.Step(0, N, 1), new ParallelOptions { MaxDegreeOfParallelism = Thread }, i =>
                for (int i = 0; i < N; i++)
                {
                    St = S0;
                    cv = 0;
                    for (int j = 1; j <= steps; j++)
                    {
                        t = (j - 1) * dt;
                        delta = BlackScholes.Delta(isput, St, t, K, sigma, T, r);
                        Stn = St * Math.Exp(nudt + sigsdt * R1[i, j - 1]);
                        cv = cv + delta * (Stn - St * erddt);
                        St = Stn;
                    }
                    CT = 0;
                    if (isput == false)
                    {
                        if (i_trails1[i] == 1)
                        {
                            CT = Math.Max(St - K, 0) + beta1 * cv;
                        }
                    }
                    else
                    {
                        if (i_trails1[i] == 1)
                        {
                            CT = Math.Max(-St + K, 0) + beta1 * cv;
                        }
                    }
                    CT0[i] = CT;
                }
                for (int i = 0; i < N; i++)
                {
                    sum_CT = sum_CT + CT0[i];
                    sum_CT2 = sum_CT2 + CT0[i] * CT0[i];
                }
                Std = Math.Sqrt((sum_CT2 - sum_CT * sum_CT / N) * Math.Exp(-2 * r * T) / (N - 1)) / Math.Sqrt(N);
            }
            else if (IsAnti == true && IsCV == true)
            {
                double[,] sims2;
                sims2 = Getsim2(steps, N, IsMT);
                double[,] i2 = new double[N, steps + 1];
                double[] i_trails2 = new double[N];
                if (S0 > barrier)
                {
                    for (int i = 0; i < N; i++)
                    {
                        i_trails2[i] = 1;
                        for (int j = 0; j <= steps; j++)
                        {
                            i2[i, j] = 1;
                            if (sims2[i, j] < barrier)
                            {
                                i2[i, j] = 0;
                            }
                            i_trails2[i] = i_trails2[i] * i2[i, j];
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < N; i++)
                    {
                        i_trails2[i] = 1;
                        for (int j = 0; j <= steps; j++)
                        {
                            i2[i, j] = 1;
                            if (sims2[i, j] > barrier)
                            {
                                i2[i, j] = 0;
                            }
                            i_trails2[i] = i_trails2[i] * i2[i, j];
                        }
                    }
                }
                double dt = T / steps, beta1 = -1;
                double nudt = (r - sigma * sigma / 2) * dt;
                double sigsdt = sigma * Math.Sqrt(dt);
                double erddt = Math.Exp(r * dt);

                double sum_CT = 0, sum_CT2 = 0;
                double St1, St2, cv1, cv2, t;
                double delta1, delta2, Stn1, Stn2;
                double CT;


                //Parallel.ForEach(IEnum.Step(0, N, 1), new ParallelOptions { MaxDegreeOfParallelism = Thread }, i =>
                for (int i = 0; i < N; i++)
                {
                    St1 = S0;
                    St2 = S0;
                    cv1 = 0;
                    cv2 = 0;
                    for (int j = 1; j <= steps; j++)
                    {
                        t = (j - 1) * dt;
                        delta1 = BlackScholes.Delta(isput, St1, t, K, sigma, T, r);
                        delta2 = BlackScholes.Delta(isput, St2, t, K, sigma, T, r);
                        Stn1 = St1 * Math.Exp(nudt + sigsdt * R1[i, j - 1]);
                        Stn2 = St2 * Math.Exp(nudt + sigsdt * (-R1[i, j - 1]));
                        cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                        cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);
                        St1 = Stn1;
                        St2 = Stn2;
                    }
                    a = 0;
                    b = 0;
                    CT = 0;
                    if (isput == false)
                    {
                        if (i_trails1[i] == 1)
                        {
                            a = St1 - K;
                            CT = CT + 0.5 * (Math.Max(0, a) + beta1 * cv1);
                        }
                        if (i_trails2[i] == 1)
                        {
                            b = St2 - K;
                            CT = CT + 0.5 * (Math.Max(0, b) + beta1 * cv2);
                        }
                    }
                    else
                    {
                        if (i_trails1[i] == 1)
                        {
                            a = -St1 + K;
                            CT = CT + 0.5 * (Math.Max(0, a) + beta1 * cv1);
                        }
                        if (i_trails2[i] == 1)
                        {
                            b = -St2 + K;
                            CT = CT + 0.5 * (Math.Max(0, b) + beta1 * cv2);
                        }
                    }
                    sum_CT = sum_CT + CT;
                    sum_CT2 = sum_CT2 + CT * CT;
                }
                double SD = Math.Sqrt((sum_CT2 - sum_CT * sum_CT / N) * Math.Exp(-2 * r * T) / (N - 1));
                Std = SD / Math.Sqrt(N);
            }

            return Std;
        }
    }

    public class RangeOption : Options
    {
        public double PriceRange(int steps, int N, Boolean IsAnti, Boolean IsCV, Boolean IsMT)
        {
            double sum = 0, price = 0;
            //We used the simulation matrix we obtained.
            double[,] sims;
            sims = Getsim(steps, N, IsMT);
            double[] S_max1 = new double[N];
            double[] S_min1 = new double[N];
            for (int i = 0; i < N; i++)
            {
                S_max1[i] = sims[i, 0];
                S_min1[i] = sims[i, 0];
                for (int j = 0; j < steps; j++)
                {
                    if (sims[i, j] > S_max1[i])
                    {
                        S_max1[i] = sims[i, j];
                    }
                    if (sims[i, j] < S_min1[i])
                    {
                        S_min1[i] = sims[i, j];
                    }
                }
            }
            if (IsAnti == false)
            {
                for (int i = 0; i < N; i++)
                {
                    sum += S_max1[i] - S_min1[i];                  
                }
                price = (sum / N) * Math.Exp(-r * T);
            }
            else if (IsAnti == true)
            {
                double[] S_max2 = new double[N];
                double[] S_min2 = new double[N];
                double[,] sims2;
                sims2 = Getsim2(steps, N, IsMT);
                for (int i = 0; i < N; i++)
                {
                    S_max2[i] = sims2[i, 0];
                    S_min2[i] = sims2[i, 0];
                    for (int j = 0; j < steps; j++)
                    {
                        if (sims2[i, j] > S_max2[i])
                        {
                            S_max2[i] = sims2[i, j];
                        }
                        if (sims2[i, j] < S_min2[i])
                        {
                            S_min2[i] = sims2[i, j];
                        }
                    }
                }
                for (int i = 0; i < N; i++)
                {
                    sum += S_max1[i] - S_min1[i];
                    sum += S_max2[i] - S_min2[i];
                }
                price = (sum / N / 2) * Math.Exp(-r * T);
            }

            return price;

        }

        public double RangeSE(int steps, int N, Boolean IsAnti, Boolean IsCV, Boolean IsMT)
        {

            double[,] sims;
            sims = Getsim(steps, N, IsMT);
            double sums = 0, Std = 0;
            double price = PriceRange(steps, N, IsAnti, IsCV, IsMT);
            double[] S_max1 = new double[N];
            double[] S_min1 = new double[N];
            for (int i = 0; i < N; i++)
            {
                S_max1[i] = sims[i, 0];
                S_min1[i] = sims[i, 0];
                for (int j = 0; j < steps; j++)
                {
                    if (sims[i, j] > S_max1[i])
                    {
                        S_max1[i] = sims[i, j];
                    }
                    if (sims[i, j] < S_min1[i])
                    {
                        S_min1[i] = sims[i, j];
                    }
                }
            }
            if (IsAnti == false && IsCV == false)
            {

                for (int i = 0; i < N; i++)
                {                   
                    sums += Math.Pow((Math.Max(-S_min1[i] + S_max1[i], 0) * Math.Exp(-r * T) - price), 2);                                      
                }
                Std = Math.Sqrt(sums / (N - 1) / N);
            }
            else if (IsAnti == true && IsCV == false)
            {
                double[] S_max2 = new double[N];
                double[] S_min2 = new double[N];
                double[,] sims2;
                sims2 = Getsim2(steps, N, IsMT);
                for (int i = 0; i < N; i++)
                {
                    S_max2[i] = sims2[i, 0];
                    S_min2[i] = sims2[i, 0];
                    for (int j = 0; j < steps; j++)
                    {
                        if (sims2[i, j] > S_max2[i])
                        {
                            S_max2[i] = sims2[i, j];
                        }
                        if (sims2[i, j] < S_min2[i])
                        {
                            S_min2[i] = sims2[i, j];
                        }
                    }
                }
                for (int i = 0; i < N; i++)
                {                    
                   sums += Math.Pow(((Math.Max(-S_min1[i] + S_max1[i], 0) + Math.Max(-S_min2[i] + S_max2[i], 0)) / 2 * Math.Exp(-r * T) - price), 2);                    
                }
                Std = Math.Sqrt(sums / (N - 1) / N);
            }
            else if (IsAnti == false && IsCV == true)
            {
                double dt = T / steps, beta1 = -1;
                double nudt = (r - sigma * sigma / 2) * dt;
                double sigsdt = sigma * Math.Sqrt(dt);
                double erddt = Math.Exp(r * dt);
                double St_sum;

                double cv, St, Stn;
                double CT, sum_CT = 0, sum_CT2 = 0;
                double[] CT0 = new double[N];
                double t, delta;
                //Parallel.ForEach(IEnum.Step(0, N, 1), new ParallelOptions { MaxDegreeOfParallelism = Thread }, i =>
                for (int i = 0; i < N; i++)
                {
                    St = S0;
                    cv = 0;
                    St_sum = St;
                    for (int j = 1; j <= steps; j++)
                    {
                        t = (j - 1) * dt;
                        delta = BlackScholes.Delta(isput, St, t, K, sigma, T, r);
                        Stn = St * Math.Exp(nudt + sigsdt * R1[i, j - 1]);
                        cv = cv + delta * (Stn - St * erddt);
                        St = Stn;
                        St_sum = St_sum + St;
                    }                   
                   CT = Math.Max(-S_min1[i] + S_max1[i], 0) + beta1 * cv;
                    
                    //sum_CT = sum_CT + CT;
                    //sum_CT2 = sum_CT2 + CT * CT;
                    CT0[i] = CT;
                }
                for (int i = 0; i < N; i++)
                {
                    sum_CT = sum_CT + CT0[i];
                    sum_CT2 = sum_CT2 + CT0[i] * CT0[i];
                }
                Std = Math.Sqrt((sum_CT2 - sum_CT * sum_CT / N) * Math.Exp(-2 * r * T) / (N - 1)) / Math.Sqrt(N);
            }
            else if (IsAnti == true && IsCV == true)
            {
                double[] S_max2 = new double[N];
                double[] S_min2 = new double[N];
                double[,] sims2;
                sims2 = Getsim2(steps, N, IsMT);
                for (int i = 0; i < N; i++)
                {
                    S_max2[i] = sims2[i, 0];
                    S_min2[i] = sims2[i, 0];
                    for (int j = 0; j < steps; j++)
                    {
                        if (sims2[i, j] > S_max2[i])
                        {
                            S_max2[i] = sims2[i, j];
                        }
                        if (sims2[i, j] < S_min2[i])
                        {
                            S_min2[i] = sims2[i, j];
                        }
                    }
                }

                double dt = T / steps, beta1 = -1;
                double nudt = (r - sigma * sigma / 2) * dt;
                double sigsdt = sigma * Math.Sqrt(dt);
                double erddt = Math.Exp(r * dt);

                double sum_CT = 0, sum_CT2 = 0;
                double St1, St2, cv1, cv2, t;
                double delta1, delta2, Stn1, Stn2;
                double CT;

                //Parallel.ForEach(IEnum.Step(0, N, 1), new ParallelOptions { MaxDegreeOfParallelism = Thread }, i =>
                for (int i = 0; i < N; i++)
                {
                    St1 = S0;
                    St2 = S0;
                    cv1 = 0;
                    cv2 = 0;
                    for (int j = 1; j <= steps; j++)
                    {
                        t = (j - 1) * dt;
                        delta1 = BlackScholes.Delta(isput, St1, t, K, sigma, T, r);
                        delta2 = BlackScholes.Delta(isput, St2, t, K, sigma, T, r);
                        Stn1 = St1 * Math.Exp(nudt + sigsdt * R1[i, j - 1]);
                        Stn2 = St2 * Math.Exp(nudt + sigsdt * (-R1[i, j - 1]));
                        cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                        cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);
                        St1 = Stn1;
                        St2 = Stn2;
                    }
                    
                     CT = 0.5 * (Math.Max(0, -S_min1[i] + S_max1[i]) + beta1 * cv1 + Math.Max(0, -S_min2[i] + S_max2[i]) + beta1 * cv2);
                                      
                    sum_CT = sum_CT + CT;
                    sum_CT2 = sum_CT2 + CT * CT;
                }
                double SD = Math.Sqrt((sum_CT2 - sum_CT * sum_CT / N) * Math.Exp(-2 * r * T) / (N - 1));
                Std = SD / Math.Sqrt(N);
            }
            return Std;
        }
    }

}
