﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;

namespace FM5092_A1
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Thread A = new Thread(RunGUI);
            A.Start();//start the thread and join
            A.Join();
        }

        public static Form1 GUI = new Form1();

        delegate void progresscheck(int amount);
        delegate void finish();

        static void RunGUI()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(GUI);//run form 1 here
        }

        public static void increase(int input)
        {
            if (Program.GUI.progressBar1.InvokeRequired)
            {
                progresscheck progressdelegate = new progresscheck(progress_check_method);
                GUI.progressBar1.BeginInvoke(progressdelegate, input);
            }
            else
            {
                progress_check_method(input);
            }
        }

        public static void finished()
        {
            if (Program.GUI.group_result.InvokeRequired)
            {
                finish finishdelegate = new finish(finish_check_method);
                GUI.group_result.BeginInvoke(finishdelegate);
            }
            else
            {
                finish_check_method();
            }
        }
        public static void progress_check_method(int amout)
        {
            GUI.progressBar1.Value += amout;
            GUI.progressBar1.Update();
        }
        public static void finish_check_method()
        {
            GUI.finish();

        }

    }
}
