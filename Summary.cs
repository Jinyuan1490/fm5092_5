﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FM5092_A1
{
    class Summary
    {
        public void Sum()
        {
            double[,] R1 = new double[IO.N, IO.steps + 1];
            R1 = RandGenerator.Randn(IO.N, IO.steps, IO.Thread);
            EuroOption O1 = new EuroOption();
            O1.S0 = IO.S0;
            O1.K = IO.K;
            O1.sigma = IO.sigma;
            O1.r = IO.r;
            O1.T = IO.T;
            O1.isput = IO.isput;
            O1.R1 = R1;

            AsianOption O2 = new AsianOption();
            O2.S0 = IO.S0;
            O2.K = IO.K;
            O2.sigma = IO.sigma;
            O2.r = IO.r;
            O2.T = IO.T;
            O2.isput = IO.isput;
            O2.R1 = R1;

            LookbackOption O4 = new LookbackOption();
            O4.S0 = IO.S0;
            O4.K = IO.K;
            O4.sigma = IO.sigma;
            O4.r = IO.r;
            O4.T = IO.T;
            O4.isput = IO.isput;
            O4.R1 = R1;

            DigitalOption O3 = new DigitalOption();
            O3.S0 = IO.S0;
            O3.K = IO.K;
            O3.sigma = IO.sigma;
            O3.r = IO.r;
            O3.T = IO.T;
            O3.isput = IO.isput;
            O3.R1 = R1;
            O3.rebate = IO.rebate;

            BarrierOption O5 = new BarrierOption();
            O5.S0 = IO.S0;
            O5.K = IO.K;
            O5.sigma = IO.sigma;
            O5.r = IO.r;
            O5.T = IO.T;
            O5.isput = IO.isput;
            O5.R1 = R1;
            O5.barrier = IO.barrier;

            RangeOption O6 = new RangeOption();
            O6.S0 = IO.S0;
            O6.K = IO.K;
            O6.sigma = IO.sigma;
            O6.r = IO.r;
            O6.T = IO.T;
            O6.isput = IO.isput;
            O6.R1 = R1;

            double a = 0, b = 0;
            double d = 0.0001;

            if (IO.Type == 1)
            {
                IO.Price = Math.Truncate(O1.PriceEuro(IO.steps, IO.N, IO.IsAnti, IO.IsCV, IO.IsMT) * 10000) / 10000;
                Program.increase(1);
                IO.SE = Math.Truncate(O1.EuroSE(IO.steps, IO.N, IO.IsAnti, IO.IsCV, IO.IsMT) * 100000) / 100000;
                Program.increase(1);
            }
            else if (IO.Type == 2)
            {
                IO.Price = Math.Truncate(O2.PriceAsian(IO.steps, IO.N, IO.IsAnti, IO.IsCV, IO.IsMT) * 10000) / 10000;
                Program.increase(1);
                IO.SE = Math.Truncate(O2.AsianSE(IO.steps, IO.N, IO.IsAnti, IO.IsCV, IO.IsMT) * 100000) / 100000;
                Program.increase(1);
            }
            else if (IO.Type == 3)
            {
                IO.Price = Math.Truncate(O3.PriceDigital(IO.steps, IO.N, IO.IsAnti, IO.IsCV, IO.IsMT) * 10000) / 10000;
                Program.increase(1);
                IO.SE = Math.Truncate(O3.DigitalSE(IO.steps, IO.N, IO.IsAnti, IO.IsCV, IO.IsMT) * 100000) / 100000;
                Program.increase(1);
                O3.S0 = IO.S0 * (1 + d);
                a = O3.PriceDigital(IO.steps, IO.N, IO.IsAnti, IO.IsCV, IO.IsMT);
                O3.S0 = IO.S0 * (1 - d);
                b = O3.PriceDigital(IO.steps, IO.N, IO.IsAnti, IO.IsCV, IO.IsMT);
            }
            else if (IO.Type == 4)
            {
                IO.Price = Math.Truncate(O4.PriceFixed(IO.steps, IO.N, IO.IsAnti, IO.IsCV, IO.IsMT) * 10000) / 10000;
                Program.increase(1);
                IO.SE = Math.Truncate(O4.FixedSE(IO.steps, IO.N, IO.IsAnti, IO.IsCV, IO.IsMT) * 100000) / 100000;
                Program.increase(1);
            }
            else if (IO.Type == 5)
            {
                IO.Price = Math.Truncate(O4.PriceFloating(IO.steps, IO.N, IO.IsAnti, IO.IsCV, IO.IsMT) * 10000) / 10000;
                Program.increase(1);
                IO.SE = Math.Truncate(O4.FloatingSE(IO.steps, IO.N, IO.IsAnti, IO.IsCV, IO.IsMT) * 100000) / 100000;
                Program.increase(1);
            }
            else if (IO.Type == 6)
            {
                IO.Price = Math.Truncate(O5.PriceIn(IO.steps, IO.N, IO.IsAnti, IO.IsCV, IO.IsMT) * 10000) / 10000;
                Program.increase(1);
                IO.SE = Math.Truncate(O5.InSE(IO.steps, IO.N, IO.IsAnti, IO.IsCV, IO.IsMT) * 100000) / 100000;
                Program.increase(1);
            }
            else if (IO.Type == 7)
            {
                IO.Price = Math.Truncate(O5.PriceOut(IO.steps, IO.N, IO.IsAnti, IO.IsCV, IO.IsMT) * 10000) / 10000;
                Program.increase(1);
                IO.SE = Math.Truncate(O5.OutSE(IO.steps, IO.N, IO.IsAnti, IO.IsCV, IO.IsMT) * 100000) / 100000;
                Program.increase(1);
            }
            else if (IO.Type == 8)
            {
                IO.Price = Math.Truncate(O6.PriceRange(IO.steps, IO.N, IO.IsAnti, IO.IsCV, IO.IsMT) * 10000) / 10000;
                Program.increase(1);
                IO.SE = Math.Truncate(O6.RangeSE(IO.steps, IO.N, IO.IsAnti, IO.IsCV, IO.IsMT) * 100000) / 100000;
                Program.increase(1);
            }
            
            IO.Delta = Math.Truncate(GreeksCalculator.Delta(IO.isput, IO.K, IO.S0, IO.sigma, IO.r, IO.T, IO.steps, IO.N, R1, IO.IsAnti, IO.IsCV, IO.IsMT, IO.Type) * 10000) / 10000;
            Program.increase(1);

            IO.Gamma = Math.Truncate(GreeksCalculator.Gamma(IO.isput, IO.K, IO.S0, IO.sigma, IO.r, IO.T, IO.steps, IO.N, R1, IO.IsAnti, IO.IsCV, IO.IsMT, IO.Type) * 10000) / 10000;
            Program.increase(1);

            IO.Vega = Math.Truncate(GreeksCalculator.Vega(IO.isput, IO.K, IO.S0, IO.sigma, IO.r, IO.T, IO.steps, IO.N, R1, IO.IsAnti, IO.IsCV, IO.IsMT, IO.Type) * 10000) / 10000;
            Program.increase(1);

            IO.Theta = Math.Truncate(GreeksCalculator.Theta(IO.isput, IO.K, IO.S0, IO.sigma, IO.r, IO.T, IO.steps, IO.N, R1, IO.IsAnti, IO.IsCV, IO.IsMT, IO.Type) * 10000) / 10000;
            Program.increase(1);

            IO.Rho = Math.Truncate(GreeksCalculator.Rho(IO.isput, IO.K, IO.S0, IO.sigma, IO.r, IO.T, IO.steps, IO.N, R1, IO.IsAnti, IO.IsCV, IO.IsMT, IO.Type) * 10000) / 10000;
            Program.increase(1);
            
        }
    }
}
