﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FM5092_A1
{
    //We created a class named GreeksCalculator to calculate the greek values.
    class GreeksCalculator
    {
        //This method is used to calculate vega.
        //Input includes, isput (call or put), underlying price S0, strike price K, volatility sigma, risk free rate r, tenor T, steps, number of trails N, and random variable matrix R.
        public static double Vega(Boolean isput, double K, double S0, double sigma, double r, double T, int steps, int N, double[,] R, Boolean IsAnti, Boolean IsCV, Boolean IsMT, int Type)
        {
            double d = 0.001 * sigma;
            double p1, p2;
            if (Type == 1)
            {
                EuroOption O1 = new EuroOption();
                O1.isput = isput;
                O1.S0 = S0;
                O1.K = K;
                O1.sigma = sigma + d * sigma;
                O1.T = T;
                O1.r = r;
                O1.R1 = R;
                p1 = O1.PriceEuro(steps, N, IsAnti, IsCV, IsMT);
                O1.sigma = sigma - d * sigma;
                p2 = O1.PriceEuro(steps, N, IsAnti, IsCV, IsMT);
            }
            else if (Type == 2)
            {
                AsianOption O1 = new AsianOption();
                O1.isput = isput;
                O1.S0 = S0;
                O1.K = K;
                O1.sigma = sigma + d * sigma;
                O1.T = T;
                O1.r = r;
                O1.R1 = R;
                //Define a is the option price with initial underlying price (1+d)*S0.
                p1 = O1.PriceAsian(steps, N, IsAnti, IsCV, IsMT);
                O1.sigma = sigma - d * sigma;
                //Define b is the option price with initial underlying price (1-d)*S0.
                p2 = O1.PriceAsian(steps, N, IsAnti, IsCV, IsMT);
            }
            else if (Type == 3)
            {
                DigitalOption O1 = new DigitalOption();
                O1.isput = isput;
                O1.S0 = S0;
                O1.K = K;
                O1.sigma = sigma + d * sigma;
                O1.T = T;
                O1.r = r;
                O1.R1 = R;
                O1.rebate = IO.rebate;
                //Define a is the option price with initial underlying price (1+d)*S0.
                p1 = O1.PriceDigital(steps, N, IsAnti, IsCV, IsMT);
                O1.sigma = sigma - d * sigma;
                //Define b is the option price with initial underlying price (1-d)*S0.
                p2 = O1.PriceDigital(steps, N, IsAnti, IsCV, IsMT);
            }
            else if (Type == 4)
            {
                LookbackOption O1 = new LookbackOption();
                O1.isput = isput;
                O1.S0 = S0;
                O1.K = K;
                O1.sigma = sigma + d * sigma;
                O1.T = T;
                O1.r = r;
                O1.R1 = R;
                //Define a is the option price with initial underlying price (1+d)*S0.
                p1 = O1.PriceFixed(steps, N, IsAnti, IsCV, IsMT);
                O1.sigma = sigma - d * sigma;
                //Define b is the option price with initial underlying price (1-d)*S0.
                p2 = O1.PriceFixed(steps, N, IsAnti, IsCV, IsMT);
            }
            else if (Type == 5)
            {
                LookbackOption O1 = new LookbackOption();
                O1.isput = isput;
                O1.S0 = S0;
                O1.K = K;
                O1.sigma = sigma + d * sigma;
                O1.T = T;
                O1.r = r;
                O1.R1 = R;
                //Define a is the option price with initial underlying price (1+d)*S0.
                p1 = O1.PriceFloating(steps, N, IsAnti, IsCV, IsMT);
                O1.sigma = sigma - d * sigma;
                //Define b is the option price with initial underlying price (1-d)*S0.
                p2 = O1.PriceFloating(steps, N, IsAnti, IsCV, IsMT);
            }
            else if (Type == 6)
            {
                BarrierOption O1 = new BarrierOption();
                O1.isput = isput;
                O1.S0 = S0;
                O1.K = K;
                O1.sigma = sigma + d * sigma;
                O1.T = T;
                O1.r = r;
                O1.R1 = R;
                O1.barrier = IO.barrier;
                //Define a is the option price with initial underlying price (1+d)*S0.
                p1 = O1.PriceIn(steps, N, IsAnti, IsCV, IsMT);
                O1.sigma = sigma - d * sigma;
                //Define b is the option price with initial underlying price (1-d)*S0.
                p2 = O1.PriceIn(steps, N, IsAnti, IsCV, IsMT);
            }
            else if (Type == 7)
            {
                BarrierOption O1 = new BarrierOption();
                O1.isput = isput;
                O1.S0 = S0;
                O1.K = K;
                O1.sigma = sigma + d * sigma;
                O1.T = T;
                O1.r = r;
                O1.R1 = R;
                O1.barrier = IO.barrier;
                //Define a is the option price with initial underlying price (1+d)*S0.
                p1 = O1.PriceOut(steps, N, IsAnti, IsCV, IsMT);
                O1.sigma = sigma - d * sigma;
                //Define b is the option price with initial underlying price (1-d)*S0.
                p2 = O1.PriceOut(steps, N, IsAnti, IsCV, IsMT);
            }
            else
            {
                RangeOption O1 = new RangeOption();
                O1.isput = isput;
                O1.S0 = S0;
                O1.K = K;
                O1.sigma = sigma + d * sigma;
                O1.T = T;
                O1.r = r;
                O1.R1 = R;
                //Define a is the option price with initial underlying price (1+d)*S0.
                p1 = O1.PriceRange(steps, N, IsAnti, IsCV, IsMT);
                O1.sigma = sigma - d * sigma;
                //Define b is the option price with initial underlying price (1-d)*S0.
                p2 = O1.PriceRange(steps, N, IsAnti, IsCV, IsMT);
            }


            return (p1 - p2) / (2 * d * sigma);
        }
        //This method is used to calculate Delta.
        //Inputs are same with above.
        public static double Delta(Boolean isput, double K, double S0, double sigma, double r, double T, int steps, int N, double[,] R, Boolean IsAnti, Boolean IsCV, Boolean IsMT, int Type)
        {
            double d = 0.001 * S0;
            double a = 0, b = 0;
            //We creat an object with the same property of user's input.
            if (Type == 1)
            {
                EuroOption O1 = new EuroOption();
                O1.isput = isput;
                O1.S0 = S0 + d;
                O1.K = K;
                O1.sigma = sigma;
                O1.T = T;
                O1.r = r;
                O1.R1 = R;
                //Define a is the option price with initial underlying price (1+d)*S0.
                a = O1.PriceEuro(steps, N, IsAnti, IsCV, IsMT);
                O1.S0 = S0 - d;
                //Define b is the option price with initial underlying price (1-d)*S0.
                b = O1.PriceEuro(steps, N, IsAnti, IsCV, IsMT);
            }
            else if(Type == 2)
            {
                AsianOption O1 = new AsianOption();
                O1.isput = isput;
                O1.S0 = S0 + d;
                O1.K = K;
                O1.sigma = sigma;
                O1.T = T;
                O1.r = r;
                O1.R1 = R;
                //Define a is the option price with initial underlying price (1+d)*S0.
                a = O1.PriceAsian(steps, N, IsAnti, IsCV, IsMT);
                O1.S0 = S0 - d;
                //Define b is the option price with initial underlying price (1-d)*S0.
                b = O1.PriceAsian(steps, N, IsAnti, IsCV, IsMT);
            }
            else if (Type == 3)
            {
                DigitalOption O1 = new DigitalOption();
                O1.isput = isput;
                O1.S0 = S0 + d;
                O1.K = K;
                O1.sigma = sigma;
                O1.T = T;
                O1.r = r;
                O1.R1 = R;
                O1.rebate = IO.rebate;
                //Define a is the option price with initial underlying price (1+d)*S0.
                a = O1.PriceDigital(steps, N, IsAnti, IsCV, IsMT);
                O1.S0 = S0 - d;
                //Define b is the option price with initial underlying price (1-d)*S0.
                b = O1.PriceDigital(steps, N, IsAnti, IsCV, IsMT);
            }
            else if (Type == 4)
            {
                LookbackOption O1 = new LookbackOption();
                O1.isput = isput;
                O1.S0 = S0 + d;
                O1.K = K;
                O1.sigma = sigma;
                O1.T = T;
                O1.r = r;
                O1.R1 = R;
                //Define a is the option price with initial underlying price (1+d)*S0.
                a = O1.PriceFixed(steps, N, IsAnti, IsCV, IsMT);
                O1.S0 = S0 - d;
                //Define b is the option price with initial underlying price (1-d)*S0.
                b = O1.PriceFixed(steps, N, IsAnti, IsCV, IsMT);
            }
            else if (Type == 5)
            {
                LookbackOption O1 = new LookbackOption();
                O1.isput = isput;
                O1.S0 = S0 + d;
                O1.K = K;
                O1.sigma = sigma;
                O1.T = T;
                O1.r = r;
                O1.R1 = R;
                //Define a is the option price with initial underlying price (1+d)*S0.
                a = O1.PriceFloating(steps, N, IsAnti, IsCV, IsMT);
                O1.S0 = S0 - d;
                //Define b is the option price with initial underlying price (1-d)*S0.
                b = O1.PriceFloating(steps, N, IsAnti, IsCV, IsMT);
            }
            else if (Type == 6)
            {
                BarrierOption O1 = new BarrierOption();
                O1.isput = isput;
                O1.S0 = S0 + d;
                O1.K = K;
                O1.sigma = sigma;
                O1.T = T;
                O1.r = r;
                O1.R1 = R;
                O1.barrier = IO.barrier;
                //Define a is the option price with initial underlying price (1+d)*S0.
                a = O1.PriceIn(steps, N, IsAnti, IsCV, IsMT);
                O1.S0 = S0 - d;
                //Define b is the option price with initial underlying price (1-d)*S0.
                b = O1.PriceIn(steps, N, IsAnti, IsCV, IsMT);
            }
            else if (Type == 7)
            {
                BarrierOption O1 = new BarrierOption();
                O1.isput = isput;
                O1.S0 = S0 + d;
                O1.K = K;
                O1.sigma = sigma;
                O1.T = T;
                O1.r = r;
                O1.R1 = R;
                O1.barrier = IO.barrier;
                //Define a is the option price with initial underlying price (1+d)*S0.
                a = O1.PriceOut(steps, N, IsAnti, IsCV, IsMT);
                O1.S0 = S0 - d;
                //Define b is the option price with initial underlying price (1-d)*S0.
                b = O1.PriceOut(steps, N, IsAnti, IsCV, IsMT);
            }
            else if (Type == 8)
            {
                RangeOption O1 = new RangeOption();
                O1.isput = isput;
                O1.S0 = S0 + d;
                O1.K = K;
                O1.sigma = sigma;
                O1.T = T;
                O1.r = r;
                O1.R1 = R;
                //Define a is the option price with initial underlying price (1+d)*S0.
                a = O1.PriceRange(steps, N, IsAnti, IsCV, IsMT);
                O1.S0 = S0 - d;
                //Define b is the option price with initial underlying price (1-d)*S0.
                b = O1.PriceRange(steps, N, IsAnti, IsCV, IsMT);
            }
         
            return (a - b) / (2 * d);
        }
        //This method is used to calculate rho.
        //Inputs are same with above.
        public static double Rho(Boolean isput, double K, double S0, double sigma, double r, double T, int steps, int N, double[,] R, Boolean IsAnti, Boolean IsCV, Boolean IsMT, int Type)
        {
            double d = 0.001 * r;
            double a, b;
            if (Type == 1)
            {
                EuroOption O1 = new EuroOption();
                O1.isput = isput;
                O1.S0 = S0;
                O1.K = K;
                O1.sigma = sigma;
                O1.T = T;
                O1.r = r + d;
                O1.R1 = R;
                //Define a is the option price with risk free rate (1+d)*r.
                a = O1.PriceEuro(steps, N, IsAnti, IsCV, IsMT);
                O1.r = r - d;
                //Define b is the option price with risk free rate (1-d)*r.
                b = O1.PriceEuro(steps, N, IsAnti, IsCV, IsMT);
            }
            else if (Type == 2)
            {
                AsianOption O1 = new AsianOption();
                O1.isput = isput;
                O1.S0 = S0;
                O1.K = K;
                O1.sigma = sigma;
                O1.T = T;
                O1.r = r + d;
                O1.R1 = R;
                //Define a is the option price with initial underlying price (1+d)*S0.
                a = O1.PriceAsian(steps, N, IsAnti, IsCV, IsMT);
                O1.r = r - d;
                //Define b is the option price with initial underlying price (1-d)*S0.
                b = O1.PriceAsian(steps, N, IsAnti, IsCV, IsMT);
            }
            else if (Type == 3)
            {
                DigitalOption O1 = new DigitalOption();
                O1.isput = isput;
                O1.S0 = S0;
                O1.K = K;
                O1.sigma = sigma;
                O1.T = T;
                O1.r = r + d;
                O1.R1 = R;
                O1.rebate = IO.rebate;
                //Define a is the option price with initial underlying price (1+d)*S0.
                a = O1.PriceDigital(steps, N, IsAnti, IsCV, IsMT);
                O1.r = r - d;
                //Define b is the option price with initial underlying price (1-d)*S0.
                b = O1.PriceDigital(steps, N, IsAnti, IsCV, IsMT);
            }
            else if (Type == 4)
            {
                LookbackOption O1 = new LookbackOption();
                O1.isput = isput;
                O1.S0 = S0;
                O1.K = K;
                O1.sigma = sigma;
                O1.T = T;
                O1.r = r + d;
                O1.R1 = R;
                //Define a is the option price with initial underlying price (1+d)*S0.
                a = O1.PriceFixed(steps, N, IsAnti, IsCV, IsMT);
                O1.r = r - d;
                //Define b is the option price with initial underlying price (1-d)*S0.
                b = O1.PriceFixed(steps, N, IsAnti, IsCV, IsMT);
            }
            else if (Type == 5)
            {
                LookbackOption O1 = new LookbackOption();
                O1.isput = isput;
                O1.S0 = S0;
                O1.K = K;
                O1.sigma = sigma;
                O1.T = T;
                O1.r = r + d;
                O1.R1 = R;
                //Define a is the option price with initial underlying price (1+d)*S0.
                a = O1.PriceFloating(steps, N, IsAnti, IsCV, IsMT);
                O1.r = r - d;
                //Define b is the option price with initial underlying price (1-d)*S0.
                b = O1.PriceFloating(steps, N, IsAnti, IsCV, IsMT);
            }
            else if (Type == 6)
            {
                BarrierOption O1 = new BarrierOption();
                O1.isput = isput;
                O1.S0 = S0;
                O1.K = K;
                O1.sigma = sigma;
                O1.T = T;
                O1.r = r + d;
                O1.R1 = R;
                O1.barrier = IO.barrier;
                //Define a is the option price with initial underlying price (1+d)*S0.
                a = O1.PriceIn(steps, N, IsAnti, IsCV, IsMT);
                O1.r = r - d;
                //Define b is the option price with initial underlying price (1-d)*S0.
                b = O1.PriceIn(steps, N, IsAnti, IsCV, IsMT);
            }
            else if (Type == 7)
            {
                BarrierOption O1 = new BarrierOption();
                O1.isput = isput;
                O1.S0 = S0;
                O1.K = K;
                O1.sigma = sigma;
                O1.T = T;
                O1.r = r + d;
                O1.R1 = R;
                O1.barrier = IO.barrier;
                //Define a is the option price with initial underlying price (1+d)*S0.
                a = O1.PriceOut(steps, N, IsAnti, IsCV, IsMT);
                O1.r = r - d;
                //Define b is the option price with initial underlying price (1-d)*S0.
                b = O1.PriceOut(steps, N, IsAnti, IsCV, IsMT);
            }
            else
            {
                RangeOption O1 = new RangeOption();
                O1.isput = isput;
                O1.S0 = S0;
                O1.K = K;
                O1.sigma = sigma;
                O1.T = T;
                O1.r = r + d;
                O1.R1 = R;
                //Define a is the option price with initial underlying price (1+d)*S0.
                a = O1.PriceRange(steps, N, IsAnti, IsCV, IsMT);
                O1.r = r - d;
                //Define b is the option price with initial underlying price (1-d)*S0.
                b = O1.PriceRange(steps, N, IsAnti, IsCV, IsMT);
            }
            return (a - b) / (2 * d);
        }
        //This method is used to calculate gamma.
        //Inputs are same with above.
        public static double Gamma(Boolean isput, double K, double S0, double sigma, double r, double T, int steps, int N, double[,] R, Boolean IsAnti, Boolean IsCV, Boolean IsMT, int Type)
        {
            double d = 0.001 * S0;
            double a, b, c;
            if (Type == 1)
            {
                EuroOption O1 = new EuroOption();
                O1.isput = isput;
                O1.S0 = S0 + d;
                O1.K = K;
                O1.sigma = sigma;
                O1.T = T;
                O1.r = r;
                O1.R1 = R;
                //Define a is the option price with initial underlying price (1+d)*S0.
                a = O1.PriceEuro(steps, N, IsAnti, IsCV, IsMT);
                O1.S0 = S0;
                //Define b is the option price with initial underlying price S0.
                b = O1.PriceEuro(steps, N, IsAnti, IsCV, IsMT);
                O1.S0 = S0 - d;
                //Define c is the option price with initial underlying price (1-d)*S0.
                c = O1.PriceEuro(steps, N, IsAnti, IsCV, IsMT);
            }
            else if (Type == 2)
            {
                AsianOption O1 = new AsianOption();
                O1.isput = isput;
                O1.S0 = S0 + d;
                O1.K = K;
                O1.sigma = sigma;
                O1.T = T;
                O1.r = r;
                O1.R1 = R;
                //Define a is the option price with initial underlying price (1+d)*S0.
                a = O1.PriceAsian(steps, N, IsAnti, IsCV, IsMT);
                O1.S0 = S0;
                //Define b is the option price with initial underlying price (1-d)*S0.
                b = O1.PriceAsian(steps, N, IsAnti, IsCV, IsMT);
                O1.S0 = S0 - d;
                //Define c is the option price with initial underlying price (1-d)*S0.
                c = O1.PriceAsian(steps, N, IsAnti, IsCV, IsMT);
            }
            else if (Type == 3)
            {
                DigitalOption O1 = new DigitalOption();
                O1.isput = isput;
                O1.S0 = S0 + d;
                O1.K = K;
                O1.sigma = sigma;
                O1.T = T;
                O1.r = r;
                O1.R1 = R;
                O1.rebate = IO.rebate;
                //Define a is the option price with initial underlying price (1+d)*S0.
                a = O1.PriceDigital(steps, N, IsAnti, IsCV, IsMT);
                O1.S0 = S0;
                //Define b is the option price with initial underlying price (1-d)*S0.
                b = O1.PriceDigital(steps, N, IsAnti, IsCV, IsMT);
                O1.S0 = S0 - d;
                //Define c is the option price with initial underlying price (1-d)*S0.
                c = O1.PriceDigital(steps, N, IsAnti, IsCV, IsMT);
            }
            else if (Type == 4)
            {
                LookbackOption O1 = new LookbackOption();
                O1.isput = isput;
                O1.S0 = S0 + d;
                O1.K = K;
                O1.sigma = sigma;
                O1.T = T;
                O1.r = r;
                O1.R1 = R;
                //Define a is the option price with initial underlying price (1+d)*S0.
                a = O1.PriceFixed(steps, N, IsAnti, IsCV, IsMT);
                O1.S0 = S0;
                //Define b is the option price with initial underlying price (1-d)*S0.
                b = O1.PriceFixed(steps, N, IsAnti, IsCV, IsMT);
                O1.S0 = S0 - d;
                //Define c is the option price with initial underlying price (1-d)*S0.
                c = O1.PriceFixed(steps, N, IsAnti, IsCV, IsMT);
            }
            else if (Type == 5)
            {
                LookbackOption O1 = new LookbackOption();
                O1.isput = isput;
                O1.S0 = S0 + d;
                O1.K = K;
                O1.sigma = sigma;
                O1.T = T;
                O1.r = r;
                O1.R1 = R;
                //Define a is the option price with initial underlying price (1+d)*S0.
                a = O1.PriceFloating(steps, N, IsAnti, IsCV, IsMT);
                O1.S0 = S0;
                //Define b is the option price with initial underlying price (1-d)*S0.
                b = O1.PriceFloating(steps, N, IsAnti, IsCV, IsMT);
                O1.S0 = S0 - d;
                //Define c is the option price with initial underlying price (1-d)*S0.
                c = O1.PriceFloating(steps, N, IsAnti, IsCV, IsMT);
            }
            else if (Type == 6)
            {
                //d = 0.01 * S0;
                BarrierOption O1 = new BarrierOption();
                O1.isput = isput;
                O1.S0 = S0 + d;
                O1.K = K;
                O1.sigma = sigma;
                O1.T = T;
                O1.r = r;
                O1.R1 = R;
                O1.barrier = IO.barrier;
                //Define a is the option price with initial underlying price (1+d)*S0.
                a = O1.PriceIn(steps, N, IsAnti, IsCV, IsMT);
                O1.S0 = S0;
                //Define b is the option price with initial underlying price (1-d)*S0.
                b = O1.PriceIn(steps, N, IsAnti, IsCV, IsMT);
                O1.S0 = S0 - d;
                //Define c is the option price with initial underlying price (1-d)*S0.
                c = O1.PriceIn(steps, N, IsAnti, IsCV, IsMT);
            }
            else if (Type == 7)
            {
                BarrierOption O1 = new BarrierOption();
                O1.isput = isput;
                O1.S0 = S0 + d;
                O1.K = K;
                O1.sigma = sigma;
                O1.T = T;
                O1.r = r;
                O1.R1 = R;
                O1.barrier = IO.barrier;
                //Define a is the option price with initial underlying price (1+d)*S0.
                a = O1.PriceOut(steps, N, IsAnti, IsCV, IsMT);
                O1.S0 = S0;
                //Define b is the option price with initial underlying price (1-d)*S0.
                b = O1.PriceOut(steps, N, IsAnti, IsCV, IsMT);
                O1.S0 = S0 - d;
                //Define c is the option price with initial underlying price (1-d)*S0.
                c = O1.PriceOut(steps, N, IsAnti, IsCV, IsMT);
            }
            else
            {
                RangeOption O1 = new RangeOption();
                O1.isput = isput;
                O1.S0 = S0 + d;
                O1.K = K;
                O1.sigma = sigma;
                O1.T = T;
                O1.r = r;
                O1.R1 = R;
                //Define a is the option price with initial underlying price (1+d)*S0.
                a = O1.PriceRange(steps, N, IsAnti, IsCV, IsMT);
                O1.S0 = S0;
                //Define b is the option price with initial underlying price (1-d)*S0.
                b = O1.PriceRange(steps, N, IsAnti, IsCV, IsMT);
                O1.S0 = S0 - d;
                //Define c is the option price with initial underlying price (1-d)*S0.
                c = O1.PriceRange(steps, N, IsAnti, IsCV, IsMT);
            }
            //Return the value of gamma.
            return (a + c - b - b) / (d * d);
        }
        //This method is used to calculate theta.
        //Inputs are same with above.
        public static double Theta(Boolean isput, double K, double S0, double sigma, double r, double T, int steps, int N, double[,] R, Boolean IsAnti, Boolean IsCV, Boolean IsMT, int Type)
        {
            //We creat an object with the same property of user's input.
            double p1, p2;
            double d = 0.01 * T;
            if (Type == 1)
            {
                EuroOption O1 = new EuroOption();
                O1.isput = isput;
                O1.S0 = S0;
                O1.K = K;
                O1.sigma = sigma;
                O1.T = T - d;
                O1.r = r;
                O1.R1 = R;
                //Define p1 as the option price with tenor T-T/steps.
                p1 = O1.PriceEuro(steps, N, IsAnti, IsCV, IsMT);
                O1.T = T + d;
                //Define p2 as the option price with tenor T.
                p2 = O1.PriceEuro(steps, N, IsAnti, IsCV, IsMT);
            }
            else if (Type == 2)
            {
                AsianOption O1 = new AsianOption();
                O1.isput = isput;
                O1.S0 = S0;
                O1.K = K;
                O1.sigma = sigma;
                O1.T = T - d;
                O1.r = r;
                O1.R1 = R;
                //Define p1 as the option price with tenor T-T/steps.
                p1 = O1.PriceAsian(steps, N, IsAnti, IsCV, IsMT);
                O1.T = T + d;
                //Define p2 as the option price with tenor T.
                p2 = O1.PriceAsian(steps, N, IsAnti, IsCV, IsMT);
            }
            else if (Type == 3)
            {
                DigitalOption O1 = new DigitalOption();
                O1.isput = isput;
                O1.S0 = S0;
                O1.K = K;
                O1.sigma = sigma;
                O1.T = T - d ;
                O1.r = r;
                O1.R1 = R;
                O1.rebate = IO.rebate;
                //Define p1 as the option price with tenor T-T/steps.
                p1 = O1.PriceDigital(steps, N, IsAnti, IsCV, IsMT);
                O1.T = T + d;
                //Define p2 as the option price with tenor T.
                p2 = O1.PriceDigital(steps, N, IsAnti, IsCV, IsMT);
            }
            else if (Type == 4)
            {
                LookbackOption O1 = new LookbackOption();
                O1.isput = isput;
                O1.S0 = S0;
                O1.K = K;
                O1.sigma = sigma;
                O1.T = T - d;
                O1.r = r;
                O1.R1 = R;
                //Define p1 as the option price with tenor T-T/steps.
                p1 = O1.PriceFixed(steps, N, IsAnti, IsCV, IsMT);
                O1.T = T + d;
                //Define p2 as the option price with tenor T.
                p2 = O1.PriceFixed(steps, N, IsAnti, IsCV, IsMT);
            }
            else if (Type == 5)
            {
                LookbackOption O1 = new LookbackOption();
                O1.isput = isput;
                O1.S0 = S0;
                O1.K = K;
                O1.sigma = sigma;
                O1.T = T - d;
                O1.r = r;
                O1.R1 = R;
                //Define p1 as the option price with tenor T-T/steps.
                p1 = O1.PriceFloating(steps, N, IsAnti, IsCV, IsMT);
                O1.T = T + d;
                //Define p2 as the option price with tenor T.
                p2 = O1.PriceFloating(steps, N, IsAnti, IsCV, IsMT);
            }
            else if (Type == 6)
            {
                BarrierOption O1 = new BarrierOption();
                O1.isput = isput;
                O1.S0 = S0;
                O1.K = K;
                O1.sigma = sigma;
                O1.T = T - T / steps;
                O1.r = r;
                O1.R1 = R;
                O1.barrier = IO.barrier;
                //Define p1 as the option price with tenor T-T/steps.
                p1 = O1.PriceIn(steps, N, IsAnti, IsCV, IsMT);
                O1.T = T + d;
                //Define p2 as the option price with tenor T.
                p2 = O1.PriceIn(steps, N, IsAnti, IsCV, IsMT);
            }
            else if (Type == 7)
            {
                
                BarrierOption O1 = new BarrierOption();
                O1.isput = isput;
                O1.S0 = S0;
                O1.K = K;
                O1.sigma = sigma;
                O1.T = T - d;
                O1.r = r;
                O1.R1 = R;
                O1.barrier = IO.barrier;
                //Define p1 as the option price with tenor T-T/steps.
                p1 = O1.PriceOut(steps, N, IsAnti, IsCV, IsMT);
                O1.T = T + d;
                //Define p2 as the option price with tenor T.
                p2 = O1.PriceOut(steps, N, IsAnti, IsCV, IsMT);
            }
            else
            {
                RangeOption O1 = new RangeOption();
                O1.isput = isput;
                O1.S0 = S0;
                O1.K = K;
                O1.sigma = sigma;
                O1.T = T - d;
                O1.r = r;
                O1.R1 = R;
                //Define p1 as the option price with tenor T-T/steps.
                p1 = O1.PriceRange(steps, N, IsAnti, IsCV, IsMT);
                O1.T = T + d;
                //Define p2 as the option price with tenor T.
                p2 = O1.PriceRange(steps, N, IsAnti, IsCV, IsMT);
            }

            return (p1 - p2) / (2 * d);
        }
    }
}
