﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FM5092_A1
{
    //Here we created a class named Simulator. Which is used to do the Monte Carlo simulation.
    class Simulator
    {
        //The method Simulate is used to do the Monte Carlo Simulation.
        //The input of this method are, underlying price S0, volatility sigma, risk free rate r, tenor T, the number of steps steps, the number of trails N,
        //and a matrix of normal distributed random variable R which is given by the RandGenerator class.
        public static double[,] Simulate(double S0, double sigma, double r, double T, int steps, int N, double[,] R, int MyThread)
        {
            double dt;
            dt = T / steps;//Here we difine the time lenth of every steps.
            //We define a N * steps+1 matrix, which will be used as output later.
            double[,] S = new double[N, steps + 1];

            Parallel.ForEach(IEnum.Step(0, N, 1), new ParallelOptions { MaxDegreeOfParallelism = MyThread }, j =>

            {
                S[j, 0] = S0;
                for (int i = 1; i <= steps; i++)
                {
                    S[j, i] = S[j, i - 1] * Math.Exp((r - sigma * sigma / 2) * dt + sigma * Math.Sqrt(dt) * R[j, i - 1]);
                }
            });

            return S;
        }
    }
}
